/*******************************************
	PostProcessArea.cpp

	Main scene and game functions
********************************************/

#include <Windows.h>
#include <sstream>
#include <string>
#include <vector>
using namespace std;

#include <d3d10.h>
#include <d3dx10.h>

#include "Defines.h"
#include "CVector3.h"
#include "CVector4.h"
#include "Camera.h"
#include "Light.h"
#include "EntityManager.h"
#include "Messenger.h"
#include "CParseLevel.h"
#include "PostProcessArea.h"

#include "BaseEffect.h"
#include "BasicEffect.h"
#include "TintEffect.h"
#include "BlurEffect.h"
#include "ShakeEffect.h"
#include "DofEffect.h"
#include "BloomEffect.h"


namespace gen
{

//*****************************************************************************
// Post-process data
//*****************************************************************************
// Post-process settings

// Separate effect file for post-processes, not necessary to use a separate file, but convenient given the architecture of this lab
ID3D10Effect* PPEffect;

// Technique name for each post-process
// Technique pointers for each post-process
vector<EPostProcesses>  CurrentTechniques;

CBaseEffect* myEffects[NumPostProcesses];

// Will render the scene to a texture in a first pass, then copy that texture to the back buffer in a second post-processing pass
// So need a texture and two "views": a render target view (to render into the texture - 1st pass) and a shader resource view (use the rendered texture as a normal texture - 2nd pass)
ID3D10Texture2D*          SceneTexture1 = NULL;
ID3D10RenderTargetView*   SceneRenderTarget1 = NULL;
ID3D10ShaderResourceView* SceneShaderResource1 = NULL;
ID3D10Texture2D*          SceneTexture2 = NULL;
ID3D10RenderTargetView*   SceneRenderTarget2 = NULL;
ID3D10ShaderResourceView* SceneShaderResource2 = NULL;

extern ID3D10ShaderResourceView* DepthStencilResource;

// Variables to link C++ post-process textures to HLSL shader variables
ID3D10EffectShaderResourceVariable* SceneTextureVar = NULL;
ID3D10EffectShaderResourceVariable* BlurredTextureVar = NULL;
ID3D10EffectShaderResourceVariable* DepthTextureVar = NULL;

// Variables specifying the area used for post-processing
ID3D10EffectVectorVariable* PPAreaTopLeftVar = NULL;
ID3D10EffectVectorVariable* PPAreaTopRightVar = NULL;
ID3D10EffectVectorVariable* PPAreaBottomLeftVar = NULL;
ID3D10EffectVectorVariable* PPAreaBottomRightVar = NULL;

// Other variables for individual post-processes
ID3D10EffectVectorVariable* TintColourVar = NULL;

ID3D10EffectScalarVariable* BlurWeightVar = NULL;
ID3D10EffectScalarVariable* BlurWidthVar = NULL;

ID3D10EffectScalarVariable* ShakeAmountVar = NULL;
ID3D10EffectScalarVariable* ShakeXMoveVar = NULL;
ID3D10EffectScalarVariable* ShakeYMoveVar = NULL;

ID3D10EffectScalarVariable* NearClipVar = NULL;
ID3D10EffectScalarVariable* FarClipVar = NULL;
ID3D10EffectScalarVariable* DoFDistanceVar = NULL;
ID3D10EffectScalarVariable* DoFRangeVar = NULL;
//*****************************************************************************


//-----------------------------------------------------------------------------
// Constants
//-----------------------------------------------------------------------------

// Control speed
const float CameraRotSpeed = 2.0f;
float CameraMoveSpeed = 80.0f;

// Amount of time to pass before calculating new average update time
const float UpdateTimePeriod = 0.25f;


//-----------------------------------------------------------------------------
// Global system variables
//-----------------------------------------------------------------------------

// Folders used for meshes/textures and effect files
extern const string MediaFolder;
extern const string ShaderFolder;

// Get reference to global DirectX variables from another source file
extern ID3D10Device*           g_pd3dDevice;
extern IDXGISwapChain*         SwapChain;
extern ID3D10DepthStencilView* DepthStencilView;
extern ID3D10RenderTargetView* BackBufferRenderTarget;
extern ID3DX10Font*            OSDFont;

// Actual viewport dimensions (fullscreen or windowed)
extern TUInt32 BackBufferWidth;
extern TUInt32 BackBufferHeight;

// Current mouse position
extern TUInt32 MouseX;
extern TUInt32 MouseY;

// Messenger class for sending messages to and between entities
extern CMessenger Messenger;


//-----------------------------------------------------------------------------
// Global game/scene variables
//-----------------------------------------------------------------------------

// Entity manager and level parser
CEntityManager EntityManager;
CParseLevel LevelParser( &EntityManager );

// Other scene elements
const int NumLights = 2;
CLight*  Lights[NumLights];
CCamera* MainCamera;
bool lightMove = true;

// Sum of recent update times and number of times in the sum - used to calculate
// average over a given time period
float SumUpdateTimes = 0.0f;
int NumUpdateTimes = 0;
float AverageUpdateTime = -1.0f; // Invalid value at first

bool AreaEffect = false;
CVector3 EffectArea = CVector3(0.0f, 0.0f, 0.0f);
bool ShowControls = false;

//-----------------------------------------------------------------------------
// Game Constants
//-----------------------------------------------------------------------------

// Lighting
const SColourRGBA AmbientColour( 0.3f, 0.3f, 0.4f, 1.0f );
CVector3 LightCentre( 0.0f, 30.0f, 50.0f );
const float LightOrbit = 170.0f;
const float LightOrbitSpeed = 0.2f;


//-----------------------------------------------------------------------------
// Scene management
//-----------------------------------------------------------------------------

bool SetArea(CVector3 pos, float width, float height);

// Creates the scene geometry
bool SceneSetup()
{
	// Prepare render methods
	InitialiseMethods();
	
	// Read templates and entities from XML file
	if (!LevelParser.ParseFile( "Entities.xml" )) return false;
	
	// Set camera position and clip planes suitable for space game
	
	MainCamera = new CCamera(CVector3(0.0f, 50, -150), CVector3(ToRadians(15.0f), 0, 0), 2.0f, 300000.0f,
		kfPi / 3.0f, static_cast<TFloat32>(BackBufferWidth) / static_cast<TFloat32>(BackBufferHeight));

	// Sunlight
	Lights[0] = new CLight( CVector3( -10000.0f, 6000.0f, 0000.0f), SColourRGBA(1.0f, 0.8f, 0.6f) * 12000, 20000.0f ); // Colour is multiplied by light brightness

	// Light orbiting area
	Lights[1] = new CLight( LightCentre, SColourRGBA(0.0f, 0.2f, 1.0f) * 50, 100.0f );

	return true;
}


// Release everything in the scene
void SceneShutdown()
{
	// Release render methods
	ReleaseMethods();

	// Release lights
	for (int light = NumLights - 1; light >= 0; --light)
	{
		delete Lights[light];
	}

	// Release camera
	delete MainCamera;

	// Destroy all entities
	EntityManager.DestroyAllEntities();
	EntityManager.DestroyAllTemplates();
}


//*****************************************************************************
// Post Processing Setup
//*****************************************************************************

// Prepare resources required for the post-processing pass
bool PostProcessSetup()
{
	// Set the current techniques array and device
	CBaseEffect::SetTechniques(&CurrentTechniques);
	CBaseEffect::SetDevice(g_pd3dDevice);

	// Set up the effects here
	myEffects[eCopy] = new CBasicEffect("PPCopy", "Copy", eCopy);
	myEffects[eCopyWorldArea] = new CBasicEffect("PPCopyWorldArea", "World Area Copy", eCopyWorldArea);
	myEffects[eNegative] = new CBasicEffect("PPNegativeColour", "Negative", eNegative);
	myEffects[eGrayscale] = new CBasicEffect("PPGrayscale", "Grayscale", eGrayscale);
	myEffects[eSepia] = new CBasicEffect("PPSepia", "Sepia", eSepia);
	myEffects[eTint] = new CTintEffect("PPTint", "Tint");
	myEffects[eBlur] = new CBlurEffect("PPBlur", "Blur");
	myEffects[eShake] = new CShakeEffect("PPShake", "Shake");
	myEffects[eDoF] = new CDoFEffect("PPDoF", "Depth of Field");
	myEffects[eBloom] = new CBloomEffect("PPBloom", "Bloom");

	// Create the "scene texture" - the texture into which the scene will be rendered in the first pass
	D3D10_TEXTURE2D_DESC textureDesc;
	textureDesc.Width  = BackBufferWidth;  // Match views to viewport size
	textureDesc.Height = BackBufferHeight;
	textureDesc.MipLevels = 1; // No mip-maps when rendering to textures (or we will have to render every level)
	textureDesc.ArraySize = 1;
	textureDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM; // RGBA texture (8-bits each)
	textureDesc.SampleDesc.Count = 1;
	textureDesc.SampleDesc.Quality = 0;
	textureDesc.Usage = D3D10_USAGE_DEFAULT;
	textureDesc.BindFlags = D3D10_BIND_RENDER_TARGET | D3D10_BIND_SHADER_RESOURCE; // Indicate we will use texture as render target, and pass it to shaders
	textureDesc.CPUAccessFlags = 0;
	textureDesc.MiscFlags = 0;
	if (FAILED(g_pd3dDevice->CreateTexture2D( &textureDesc, NULL, &SceneTexture1 ))) return false;
	if (FAILED(g_pd3dDevice->CreateTexture2D( &textureDesc, NULL, &SceneTexture2 ))) return false;

	// Get a "view" of the texture as a render target - giving us an interface for rendering to the texture
	if (FAILED(g_pd3dDevice->CreateRenderTargetView( SceneTexture1, NULL, &SceneRenderTarget1 ))) return false;
	if (FAILED(g_pd3dDevice->CreateRenderTargetView( SceneTexture2, NULL, &SceneRenderTarget2 ))) return false;

	// And get a shader-resource "view" - giving us an interface for passing the texture to shaders
	D3D10_SHADER_RESOURCE_VIEW_DESC srDesc;
	srDesc.Format = textureDesc.Format;
	srDesc.ViewDimension = D3D10_SRV_DIMENSION_TEXTURE2D;
	srDesc.Texture2D.MostDetailedMip = 0;
	srDesc.Texture2D.MipLevels = 1;
	if (FAILED(g_pd3dDevice->CreateShaderResourceView( SceneTexture1, &srDesc, &SceneShaderResource1 ))) return false;
	if (FAILED(g_pd3dDevice->CreateShaderResourceView( SceneTexture2, &srDesc, &SceneShaderResource2 ))) return false;

	// Load and compile a separate effect file for post-processes.
	ID3D10Blob* pErrors;
	DWORD dwShaderFlags = D3D10_SHADER_ENABLE_STRICTNESS; // These "flags" are used to set the compiler options

	string fullFileName = ShaderFolder + "PostProcess.fx";
	if( FAILED( D3DX10CreateEffectFromFile( fullFileName.c_str(), NULL, NULL, "fx_4_0", dwShaderFlags, 0, g_pd3dDevice, NULL, NULL, &PPEffect, &pErrors, NULL ) ))
	{
		if (pErrors != 0)  MessageBox( NULL, reinterpret_cast<char*>(pErrors->GetBufferPointer()), "Error", MB_OK ); // Compiler error: display error message
		else               MessageBox( NULL, "Error loading FX file. Ensure your FX file is in the same folder as this executable.", "Error", MB_OK );  // No error message - probably file not found
		return false;
	}

	// There's an array of post-processing technique names above - get array of post-process techniques matching those names from the compiled effect file
	for (int pp = 0; pp < NumPostProcesses; pp++)
	{
		myEffects[pp]->SetEffectTechnique(PPEffect->GetTechniqueByName(myEffects[pp]->GetPPTechName().c_str()));
	}
	
	// Link to HLSL variables in post-process shaders
	SceneTextureVar      = PPEffect->GetVariableByName( "SceneTexture" )->AsShaderResource();
	BlurredTextureVar	 = PPEffect->GetVariableByName( "BlurredTexture" )->AsShaderResource();
	DepthTextureVar		 = PPEffect->GetVariableByName( "DepthTexture" )->AsShaderResource();
	PPAreaTopLeftVar     = PPEffect->GetVariableByName( "PPAreaTopLeft" )->AsVector();
	PPAreaTopRightVar    = PPEffect->GetVariableByName( "PPAreaTopRight" )->AsVector();
	PPAreaBottomLeftVar  = PPEffect->GetVariableByName( "PPAreaBottomLeft" )->AsVector();
	PPAreaBottomRightVar = PPEffect->GetVariableByName( "PPAreaBottomRight" )->AsVector();
	TintColourVar        = PPEffect->GetVariableByName( "TintColour" )->AsVector();
	BlurWeightVar		 = PPEffect->GetVariableByName( "BlurWeight" )->AsScalar();
	BlurWidthVar		 = PPEffect->GetVariableByName( "BlurWidth" )->AsScalar();
	ShakeAmountVar       = PPEffect->GetVariableByName( "ShakeAmount" )->AsScalar();
	ShakeXMoveVar		 = PPEffect->GetVariableByName( "ShakeXMove" )->AsScalar();
	ShakeYMoveVar		 = PPEffect->GetVariableByName( "ShakeYMove" )->AsScalar();
	NearClipVar			 = PPEffect->GetVariableByName( "NearClip" )->AsScalar();
	FarClipVar			 = PPEffect->GetVariableByName( "FarClip" )->AsScalar();
	DoFDistanceVar		 = PPEffect->GetVariableByName( "DoFDistance" )->AsScalar();
	DoFRangeVar			 = PPEffect->GetVariableByName( "DoFRange" )->AsScalar();

	// Set the variables in the static classes
	CBaseEffect::SetSceneTextureVar(SceneTextureVar);
	CBaseEffect::SetSceneRenderTarget1(SceneRenderTarget1);
	CBaseEffect::SetSceneRenderTarget2(SceneRenderTarget2);
	CBaseEffect::SetSceneShaderResource1(SceneShaderResource1);
	CBaseEffect::SetSceneShaderResource2(SceneShaderResource2);
	CBaseEffect::SetDepthStencilView(DepthStencilView);

	CDoFEffect::SetNearFarClip(MainCamera->GetNearClip(), MainCamera->GetFarClip());
	CDoFEffect::SetVariables(BlurredTextureVar, DepthTextureVar, NearClipVar, FarClipVar, DoFDistanceVar, DoFRangeVar);
	CDoFEffect::SetDepthStencilResource(DepthStencilResource);
	CDoFEffect::CreateTexture(BackBufferWidth, BackBufferHeight);
	CDoFEffect::SetBlurEffect(static_cast<CBlurEffect*>(myEffects[eBlur]));

	CBloomEffect::SetVariables(BlurredTextureVar, DepthTextureVar);
	CBloomEffect::SetDepthStencilResource(DepthStencilResource);
	CBloomEffect::CreateTexture(BackBufferWidth, BackBufferHeight);
	CBloomEffect::SetBlurEffect(static_cast<CBlurEffect*>(myEffects[eBlur]));

	CTintEffect::LoadVariables(TintColourVar);
	CBlurEffect::LoadVariables(BlurWeightVar, BlurWidthVar);
	CShakeEffect::LoadVariables(ShakeAmountVar, ShakeXMoveVar, ShakeYMoveVar);

	return true;
}

void PostProcessShutdown()
{
	
	if (PPEffect)            PPEffect->Release();
	if (SceneShaderResource1) SceneShaderResource1->Release();
	if (SceneRenderTarget1)   SceneRenderTarget1->Release();
	if (SceneTexture1)        SceneTexture1->Release();
	if (SceneShaderResource2) SceneShaderResource2->Release();
	if (SceneRenderTarget2)   SceneRenderTarget2->Release();
	if (SceneTexture2)        SceneTexture2->Release();
	for (int pp = 0; pp < NumPostProcesses; pp++)
	{
		if (myEffects[pp])  myEffects[pp]->Release();
		if (myEffects[pp])	delete(myEffects[pp]);
	}
}

//*****************************************************************************


//-----------------------------------------------------------------------------
// Post Process Setup / Update
//-----------------------------------------------------------------------------
// Separated out from RenderScene for clarity and flexibility

// Update post-processes (those that need updating) during scene update
void UpdatePostProcesses( float updateTime )
{
	// Tint
	myEffects[eTint]->Update(updateTime);

	// Shake
	myEffects[eShake]->Update(updateTime);
}


//**************************************************
// AREA VERTEX AND TEXTURE COORDINATE CALCULATIONS

// Sets in the shaders the top-left, bottom-right and depth coordinates of the area post process to work on
// Requires a world point at the centre of the area, the width and height of the area (in world units), an optional depth offset (to pull or push 
// the effect of the post-processing into the scene). Also requires the camera, since we are creating a camera facing quad.
void SetPostProcessCameraArea( CCamera* camera, CVector3 areaCentre, float width, float height, float depthOffset = 0.0f )
{
	// Turn areaCentre (a point in world space) into a CVector4. Then multiply it by the camera's view matrix to get the area centre in camera space.
	// Put this result in a CVector4 variable called cameraSpaceCentre
	CVector4 cameraSpaceCentre = CVector4(areaCentre, 1.0f) * camera->GetViewMatrix();

	// Subtract width/2 from cameraSpaceCentre's x coordinate. Then add height/2 from the y coordinate (y axis goes up). The resulting point is at the
	// top left of a camera facing quad. We work in camera space because its axes are camera aligned so creating a camera facing quad is easy.
	// Copy the resulting point into a new CVector4 variable called cameraTopLeft
	cameraSpaceCentre.x -= width / 2;
	cameraSpaceCentre.y += height / 2;

	CVector4 cameraTopLeft = cameraSpaceCentre;

	// In a similar way caclulate a new CVector4 variable called cameraBottomRight. Be careful to add/subtract the correct amounts.
	cameraSpaceCentre.x += width;
	cameraSpaceCentre.y -= height;

	CVector4 cameraBottomRight = cameraSpaceCentre;

	// Multiply both cameraTopLeft and cameraBottomRight by the camera's projection matrix and put the results in new CVector4 variables
	// called projTopLeft and projBottomRight. This gives the projection space coordinates of the post process area
	CVector4 projTopLeft = cameraTopLeft * camera->GetProjMatrix();
	CVector4 projBottomRight = cameraBottomRight * camera->GetProjMatrix();

	// Divide the x & y coordinates of both projTopLeft and projTopLeft by the w coordinate. This is the perspective divide. We are doing
	// this manually to create 2D coordinates in viewport space, ranging from -1.0 to 1.0 from left->right and bottom->top of the viewport.
	// These coordinates will be used in the vertex shader to create both vertex and texture coordinates for the quad to render
	projTopLeft.x /= projTopLeft.w;
	projTopLeft.y /= projTopLeft.w;

	projBottomRight.x /= projBottomRight.w;
	projBottomRight.y /= projBottomRight.w;

	// Add the depthOffset variable to both the z and w coordinates of projTopLeft. No need to do projBottomRight, as it will be the same.
	// Then divide the z coordinate by the w coordinate. This is the final part of the perspective divide to give the depth buffer value,
	// but here tweaked with a depth offset. This method is an approximation but will work fine with typical camera settings
	projTopLeft.w += depthOffset;
	projTopLeft.z = (projTopLeft.z + depthOffset) / projTopLeft.w;

	// Negate the y coordinates of both projTopLeft and projTopLeft. The divide the x & y coordinates of both by 2.0f and add 0.5f.
	// This converts the viewport coordinates (-1 to 1 range) into UV coordinates (0 to 1 range, y down). In fact, we will need the
	// coordinates in both ranges (viewport coordinates for rendering the quad, UV coordinates for sampling the scene texture), but
	// we do this conversion here as it helps some shaders to have the post-processing area in UV space (e.g. spiral needs to know
	// the centre of the spiralling area in the scene texture)
	projTopLeft.y = -projTopLeft.y;
	projBottomRight.y = -projBottomRight.y;

	projTopLeft.x = (projTopLeft.x / 2.0f) + 0.5f;
	projTopLeft.y = (projTopLeft.y / 2.0f) + 0.5f;

	projBottomRight.x = (projBottomRight.x / 2.0f) + 0.5f;
	projBottomRight.y = (projBottomRight.y / 2.0f) + 0.5f;

	// Send the values calculated to the shader (this part is done). The post-processing vertex shader needs only these values to
	// create the vertex buffer for the quad to render, we don't need to create a vertex buffer for post-processing at all.
	PPAreaTopLeftVar->SetRawValue( &projTopLeft.Vector3(), 0, 12 );         // Viewport space x & y for top-left
	PPAreaBottomRightVar->SetRawValue( &projBottomRight.Vector3(), 0, 12 ); // Same for bottom-right
}

// Set the top-left, bottom-right and depth coordinates for the area post process to work on for full-screen processing
// Since all post process code is now area-based, full-screen processing needs to explicitly set up the appropriate full-screen rectangle
void SetFullScreenPostProcessArea()
{
	CVector3 TopLeftUV     = CVector3( 0.0f, 0.0f, 0.0f ); // Top-left and bottom-right in UV space
	CVector3 BottomRightUV = CVector3( 1.0f, 1.0f, 0.0f );

	PPAreaTopLeftVar->SetRawValue( &TopLeftUV, 0, 12 );
	PPAreaBottomRightVar->SetRawValue( &BottomRightUV, 0, 12 );
}

//**************************************************

void SetPostProcessWorldArea(CCamera* camera, CVector3 areaCentre, float width, float height, float depthOffset = 0.0f)
{

	// Turn areaCentre (a point in world space) into a CVector4. Then multiply it by the camera's view matrix to get the area centre in camera space.
	// Put this result in a CVector4 variable called cameraSpaceCentre
	CVector4 areaTopLeft = CVector4(areaCentre, 1.0f);
	CVector4 areaTopRight = CVector4(areaCentre, 1.0f);
	CVector4 areaBottomLeft = CVector4(areaCentre, 1.0f);
	CVector4 areaBottomRight = CVector4(areaCentre, 1.0f);

	// Subtract width/2 from cameraSpaceCentre's x coordinate. Then add height/2 from the y coordinate (y axis goes up). The resulting point is at the
	// top left of a camera facing quad. We work in camera space because its axes are camera aligned so creating a camera facing quad is easy.
	// Copy the resulting point into a new CVector4 variable called cameraTopLeft
	float halfWidth = width / 2;
	float halfHeight = height / 2;

	areaTopLeft.x -= halfWidth;
	areaTopLeft.y += halfHeight;

	areaTopRight.x += halfWidth;
	areaTopRight.y += halfHeight;

	areaBottomLeft.x -= halfWidth;
	areaBottomLeft.y -= halfHeight;

	areaBottomRight.x += halfWidth;
	areaBottomRight.y -= halfHeight;

	CVector4 cameraTopLeft = areaTopLeft * camera->GetViewMatrix();
	CVector4 cameraTopRight = areaTopRight * camera->GetViewMatrix();

	CVector4 cameraBottomLeft = areaBottomLeft * camera->GetViewMatrix();
	CVector4 cameraBottomRight = areaBottomRight * camera->GetViewMatrix();

	// Multiply both cameraTopLeft and cameraBottomRight by the camera's projection matrix and put the results in new CVector4 variables
	// called projTopLeft and projBottomRight. This gives the projection space coordinates of the post process area
	CVector4 projTopLeft = cameraTopLeft * camera->GetProjMatrix();
	CVector4 projTopRight = cameraTopRight * camera->GetProjMatrix();
	CVector4 projBottomLeft = cameraBottomLeft * camera->GetProjMatrix();
	CVector4 projBottomRight = cameraBottomRight * camera->GetProjMatrix();

	// Divide the x & y coordinates of both projTopLeft and projTopLeft by the w coordinate. This is the perspective divide. We are doing
	// this manually to create 2D coordinates in viewport space, ranging from -1.0 to 1.0 from left->right and bottom->top of the viewport.
	// These coordinates will be used in the vertex shader to create both vertex and texture coordinates for the quad to render
	projTopLeft.x /= projTopLeft.w;
	projTopLeft.y /= projTopLeft.w;

	projTopRight.x /= projTopRight.w;
	projTopRight.y /= projTopRight.w;

	projBottomLeft.x /= projBottomLeft.w;
	projBottomLeft.y /= projBottomLeft.w;

	projBottomRight.x /= projBottomRight.w;
	projBottomRight.y /= projBottomRight.w;

	// Add the depthOffset variable to both the z and w coordinates of projTopLeft. No need to do projBottomRight, as it will be the same.
	// Then divide the z coordinate by the w coordinate. This is the final part of the perspective divide to give the depth buffer value,
	// but here tweaked with a depth offset. This method is an approximation but will work fine with typical camera settings
	projTopLeft.w += depthOffset;
	projTopLeft.z = (projTopLeft.z + depthOffset) / projTopLeft.w;

	projTopRight.w += depthOffset;
	projTopRight.z = (projTopRight.z + depthOffset) / projTopRight.w;

	projBottomLeft.w += depthOffset;
	projBottomLeft.z = (projBottomLeft.z + depthOffset) / projBottomLeft.w;

	projBottomRight.w += depthOffset;
	projBottomRight.z = (projBottomRight.z + depthOffset) / projBottomRight.w;

	// Negate the y coordinates of both projTopLeft and projTopLeft. The divide the x & y coordinates of both by 2.0f and add 0.5f.
	// This converts the viewport coordinates (-1 to 1 range) into UV coordinates (0 to 1 range, y down). In fact, we will need the
	// coordinates in both ranges (viewport coordinates for rendering the quad, UV coordinates for sampling the scene texture), but
	// we do this conversion here as it helps some shaders to have the post-processing area in UV space (e.g. spiral needs to know
	// the centre of the spiralling area in the scene texture)
	projTopLeft.y = -projTopLeft.y;
	projTopRight.y = -projTopRight.y;
	projBottomLeft.y = -projBottomLeft.y;
	projBottomRight.y = -projBottomRight.y;

	projTopLeft.x = (projTopLeft.x / 2.0f) + 0.5f;
	projTopLeft.y = (projTopLeft.y / 2.0f) + 0.5f;

	projTopRight.x = (projTopRight.x / 2.0f) + 0.5f;
	projTopRight.y = (projTopRight.y / 2.0f) + 0.5f;

	projBottomLeft.x = (projBottomLeft.x / 2.0f) + 0.5f;
	projBottomLeft.y = (projBottomLeft.y / 2.0f) + 0.5f;

	projBottomRight.x = (projBottomRight.x / 2.0f) + 0.5f;
	projBottomRight.y = (projBottomRight.y / 2.0f) + 0.5f;

	// Send the values calculated to the shader (this part is done). The post-processing vertex shader needs only these values to
	// create the vertex buffer for the quad to render, we don't need to create a vertex buffer for post-processing at all.
	PPAreaTopLeftVar->SetRawValue(&projTopLeft.Vector3(), 0, 12);         // Viewport space x & y for top-left
	PPAreaTopRightVar->SetRawValue(&projTopRight.Vector3(), 0, 12);
	PPAreaBottomLeftVar->SetRawValue(&projBottomLeft.Vector3(), 0, 12);
	PPAreaBottomRightVar->SetRawValue(&projBottomRight.Vector3(), 0, 12); // Same for bottom-right
}


//-----------------------------------------------------------------------------
// Game loop functions
//-----------------------------------------------------------------------------

// Draw one frame of the scene
void RenderScene()
{
	// Setup the viewport - defines which part of the back-buffer we will render to (usually all of it)
	D3D10_VIEWPORT vp;
	vp.Width  = BackBufferWidth;
	vp.Height = BackBufferHeight;
	vp.MinDepth = 0.0f;
	vp.MaxDepth = 1.0f;
	vp.TopLeftX = 0;
	vp.TopLeftY = 0;
	g_pd3dDevice->RSSetViewports( 1, &vp );


	//------------------------------------------------
	// SCENE RENDER PASS - rendering to a texture

	// Specify that we will render to the scene texture in this first pass (rather than the backbuffer), will share the depth/stencil buffer with the backbuffer though

	if (CBaseEffect::CurrentTarget() == 2)
	{
		CBaseEffect::SwitchCurrentTarget();
	}
	g_pd3dDevice->OMSetRenderTargets(1, &SceneRenderTarget1, DepthStencilView);


	// Clear the texture and the depth buffer
	g_pd3dDevice->ClearRenderTargetView( SceneRenderTarget1, &AmbientColour.r );
	g_pd3dDevice->ClearRenderTargetView( SceneRenderTarget2, &AmbientColour.r);
	g_pd3dDevice->ClearDepthStencilView( DepthStencilView, D3D10_CLEAR_DEPTH, 1.0f, 0 );

	// Prepare camera
	MainCamera->SetAspect( static_cast<TFloat32>(BackBufferWidth) / BackBufferHeight );
	MainCamera->CalculateMatrices();
	MainCamera->CalculateFrustrumPlanes();

	// Set camera and light data in shaders
	SetCamera( MainCamera );
	SetAmbientLight( AmbientColour );
	SetLights( &Lights[0] );

	// Render entities
	EntityManager.RenderAllEntities( MainCamera );

	CBaseEffect::SwitchCurrentTarget();

	//************************************************
	// AREA POST PROCESS RENDER PASS - Render smaller quad on the back-buffer mapped with a matching area of the scene texture, with different post-processing

	// Set the area size, 20 units wide and high, 0 depth offset. This sets up a viewport space quad for the post-process to work on
	// Note that the function needs the camera to turn the point into a camera facing rectangular area
	g_pd3dDevice->IASetInputLayout(NULL);
	g_pd3dDevice->IASetPrimitiveTopology(D3D10_PRIMITIVE_TOPOLOGY_TRIANGLESTRIP);

	// Select one of the post-processing techniques and render the area using it
	// Render to target 2 first, then 1 etc
	// Swap the SceneTextVar each time

	for (vector<EPostProcesses>::iterator techniqueIter = CurrentTechniques.begin(); techniqueIter != CurrentTechniques.end(); ++techniqueIter)
	{		
		myEffects[*techniqueIter]->SetVariables();
		
		if (myEffects[*techniqueIter]->GetAreaPP())
		{
			for (unsigned int pass = 0; pass < myEffects[*techniqueIter]->GetPasses(); pass++)
			{
				// Render the technique to full screen then copy it onto the first render with a copy
				SetFullScreenPostProcessArea();
				myEffects[*techniqueIter]->Render(pass);
				CBaseEffect::SwitchCurrentTarget();

				SetPostProcessWorldArea(MainCamera, EffectArea, 20.0f, 20.0f, 0);
				myEffects[eCopyWorldArea]->Render(0);
				CBaseEffect::SwitchCurrentTarget();
			}
		}
		else
		{
			SetFullScreenPostProcessArea();

			for (unsigned int pass = 0; pass < myEffects[*techniqueIter]->GetPasses(); pass++)
			{
				myEffects[*techniqueIter]->Render(pass);
				CBaseEffect::SwitchCurrentTarget();
			}
		}
		
	}

	//************************************************
	// Finally copy everything over to the back buffer
	SetFullScreenPostProcessArea();

	// Select the back buffer to use for rendering (will ignore depth-buffer for full-screen quad) and select scene texture for use in shader
	g_pd3dDevice->IASetInputLayout(NULL);
	g_pd3dDevice->IASetPrimitiveTopology(D3D10_PRIMITIVE_TOPOLOGY_TRIANGLESTRIP);
	g_pd3dDevice->OMSetRenderTargets(1, &BackBufferRenderTarget, DepthStencilView); // No need to clear the back-buffer, we're going to overwrite it all


	if (CBaseEffect::CurrentTarget() == 1)
	{
		SceneTextureVar->SetResource(SceneShaderResource2);
	}
	else
	{
		SceneTextureVar->SetResource(SceneShaderResource1);
	}

	myEffects[eCopy]->GetEffectTechnique()->GetPassByIndex(0)->Apply(0);
	g_pd3dDevice->Draw(4, 0);

	// These two lines unbind the scene texture from the shader to stop DirectX issuing a warning when we try to render to it again next frame
	SceneTextureVar->SetResource( 0 );

	// Render UI elements last - don't want them post-processed
	RenderSceneText();

	// Present the backbuffer contents to the display
	SwapChain->Present( 0, 0 );
}


// Render a single text string at the given position in the given colour, may optionally centre it
void RenderText( const string& text, int X, int Y, float r, float g, float b, bool centre = false )
{
	RECT rect;
	if (!centre)
	{
		SetRect( &rect, X, Y, 0, 0 );
		OSDFont->DrawText( NULL, text.c_str(), -1, &rect, DT_NOCLIP, D3DXCOLOR( r, g, b, 1.0f ) );
	}
	else
	{
		SetRect( &rect, X - 100, Y, X + 100, 0 );
		OSDFont->DrawText( NULL, text.c_str(), -1, &rect, DT_CENTER | DT_NOCLIP, D3DXCOLOR( r, g, b, 1.0f ) );
	}
}

// Render on-screen text each frame
void RenderSceneText()
{
	// Write FPS text string
	stringstream outText;
	if (AverageUpdateTime >= 0.0f)
	{
		outText << "Frame Time: " << AverageUpdateTime * 1000.0f << "ms" << endl << "FPS:" << 1.0f / AverageUpdateTime;
		RenderText( outText.str(), 2, 2, 0.0f, 0.0f, 0.0f );
		RenderText( outText.str(), 0, 0, 1.0f, 1.0f, 0.0f );
		outText.str("");
	}

	// Output post-process name
	if (AreaEffect)
	{
		outText << "Area Mode" << endl << endl;
	}
	else
	{
		outText << "Fullscreen Mode" << endl << endl;
	}
	outText << "Fullscreen Post-Processes: " << endl;

	// Just copy if no technique is selected
	if (CurrentTechniques.empty())
	{
		outText << myEffects[eCopy]->GetTechName();
	}
	else
	{
		// Output all the full screen effects first then the area ones if there is any
		bool area = false;
		for (vector<EPostProcesses>::iterator itr = CurrentTechniques.begin(); itr != CurrentTechniques.end(); ++itr)
		{
			if (!myEffects[*itr]->GetAreaPP())
			{
				outText << myEffects[*itr]->GetTechName() << myEffects[*itr]->OutInfo() << endl;
			}
			else
			{
				area = true;
			}
		}

		if (area)
		{
			outText << endl << "Area Post-Processes: " << endl;
			for (vector<EPostProcesses>::iterator itr = CurrentTechniques.begin(); itr != CurrentTechniques.end(); ++itr)
			{
				if (myEffects[*itr]->GetAreaPP())
				{
					outText << myEffects[*itr]->GetTechName() << myEffects[*itr]->OutInfo() << endl;
				}
			}
		}
	}


	RenderText( outText.str(),  0, 32,  1.0f, 1.0f, 1.0f );

	stringstream controlstream;

	if (ShowControls)
	{
		controlstream << "F1 - Toggle Fullscreen" << endl;
		controlstream << "F2 - Camera Move Speed = 5.0f" << endl;
		controlstream << "F3 - Camera Move Speed = 40.0f" << endl;
		controlstream << "F4 - Camera Move Speed = 160.0f" << endl;
		controlstream << "F5 - Camera Move Speed = 640.0f" << endl;
		controlstream << "0 - Toggle Light Movement" << endl << endl;

		controlstream << "Q - Toggle Area Select" << endl;
		controlstream << "LMB - Select Area" << endl << endl;
		controlstream << "1 - Toggle Tint" << endl;
		controlstream << "2 - Toggle Blur" << endl;
		controlstream << "3 - Toggle Shake" << endl;
		controlstream << "4 - Toggle Depth of Field" << endl;
		controlstream << "B - Toggle Bloom" << endl << endl;

		controlstream << "N - Toggle Negative" << endl;
		controlstream << "M - Toggle Sepia" << endl;
		controlstream << "G - Toggle Grayscale" << endl << endl;

		controlstream << "O - Increase DoF Distance" << endl;
		controlstream << "I - Decrease DoF Distance" << endl;
		controlstream << "L - Decrease DoF Range" << endl;
		controlstream << "K - Increase DoF Range" << endl;

		controlstream << "X - Increase Blur Kernel Size" << endl;
		controlstream << "Z - Reduce Blur Kernel Size" << endl;
		controlstream << "+ - Increase Blur Sigma" << endl;
		controlstream << "- - Reduce Blur Sigma" << endl;
	}
	else
	{
		controlstream << "C - Show Controls";
	}

	RenderText(controlstream.str(), BackBufferWidth - 180, 2, 1.0f, 1.0f, 1.0f);
}


// Update the scene between rendering
void UpdateScene( float updateTime )
{
	// Call all entity update functions
	EntityManager.UpdateAllEntities( updateTime );

	// Update any post processes that need updates
	UpdatePostProcesses( updateTime );

	// Set camera speeds
	// Key F1 used for full screen toggle
	if (KeyHit( Key_F2 )) CameraMoveSpeed = 5.0f;
	if (KeyHit( Key_F3 )) CameraMoveSpeed = 40.0f;
	if (KeyHit( Key_F4 )) CameraMoveSpeed = 160.0f;
	if (KeyHit( Key_F5 )) CameraMoveSpeed = 640.0f;

	if (KeyHit(Key_Q))
	{
		AreaEffect = !AreaEffect;
	}
	if (KeyHit(Key_C))
	{
		ShowControls = !ShowControls;
	}

	// Choose post-process
	//Tint
	if (KeyHit(Key_1))
	{
		myEffects[eTint]->SwitchProcess();
		if (AreaEffect)
		{
			myEffects[eTint]->AreaPPOn();
		}
		else
		{
			myEffects[eTint]->AreaPPOff();
		}
	}
	// Blur
	if (KeyHit(Key_2))
	{
		myEffects[eBlur]->SwitchProcess();
		if (AreaEffect)
		{
			myEffects[eBlur]->AreaPPOn();
		}
		else
		{
			myEffects[eBlur]->AreaPPOff();
		}
	}
	// Shake
	if (KeyHit(Key_3))
	{
		if (myEffects[eShake]->SwitchProcess())
		{
			static_cast<CShakeEffect*>(myEffects[eShake])->ResetShake();
		}
		if (AreaEffect)
		{
			myEffects[eShake]->AreaPPOn();
		}
		else
		{
			myEffects[eShake]->AreaPPOff();
		}
	}

	// Negative
	if (KeyHit(Key_N))
	{
		myEffects[eNegative]->SwitchProcess();
		if (AreaEffect)
		{
			myEffects[eNegative]->AreaPPOn();
		}
		else
		{
			myEffects[eNegative]->AreaPPOff();
		}
	}

	// Sepia
	if (KeyHit(Key_M))
	{
		myEffects[eSepia]->SwitchProcess();
		if (AreaEffect)
		{
			myEffects[eSepia]->AreaPPOn();
		}
		else
		{
			myEffects[eSepia]->AreaPPOff();
		}
	}

	// Grayscale
	if (KeyHit(Key_G))
	{
		myEffects[eGrayscale]->SwitchProcess();
		if (AreaEffect)
		{
			myEffects[eGrayscale]->AreaPPOn();
		}
		else
		{
			myEffects[eGrayscale]->AreaPPOff();
		}
	}

	// Depth of Field
	if (KeyHit(Key_4))
	{
		myEffects[eDoF]->SwitchProcess();
		if (AreaEffect)
		{
			myEffects[eDoF]->AreaPPOn();
		}
		else
		{
			myEffects[eDoF]->AreaPPOff();
		}
	}

	// Bloom
	if (KeyHit(Key_B))
	{
		myEffects[eBloom]->SwitchProcess();
		if (AreaEffect)
		{
			myEffects[eBloom]->AreaPPOn();
		}
		else
		{
			myEffects[eBloom]->AreaPPOff();
		}
	}

	// The controls for changing effect variables 
	// Decrease DoF distance, limit to 0
	if (KeyHeld(Key_I))
	{
		float DoFDistance = static_cast<CDoFEffect*>(myEffects[eDoF])->GetDoFDistance();
		DoFDistance = max(DoFDistance - 1.0f, 0.0f);
		static_cast<CDoFEffect*>(myEffects[eDoF])->SetDoFDistance(DoFDistance);
	}
	// Increase DoF distance
	if (KeyHeld(Key_O))
	{
		float DoFDistance = static_cast<CDoFEffect*>(myEffects[eDoF])->GetDoFDistance();
		DoFDistance += 1.0f;
		static_cast<CDoFEffect*>(myEffects[eDoF])->SetDoFDistance(DoFDistance);
	}

	// Decrease DoF range
	if (KeyHeld(Key_K))
	{
		float DoFRange = static_cast<CDoFEffect*>(myEffects[eDoF])->GetDoFRange();
		DoFRange = max(DoFRange - 1.0f, 0.0f);
		static_cast<CDoFEffect*>(myEffects[eDoF])->SetDoFRange(DoFRange);
	}
	// Increase DoF range
	if (KeyHeld(Key_L))
	{
		float DoFRange = static_cast<CDoFEffect*>(myEffects[eDoF])->GetDoFRange();
		DoFRange += 1.0f;
		static_cast<CDoFEffect*>(myEffects[eDoF])->SetDoFRange(DoFRange);
	}

	// Increase blur sigma
	if (KeyHit(Key_Add))
	{
		float newSigma = static_cast<CBlurEffect*>(myEffects[eBlur])->GetSigma() + 0.5f;
		if (newSigma > 10.0f)
		{
			newSigma = 10.0f;
		}
		static_cast<CBlurEffect*>(myEffects[eBlur])->ChangeSigma(newSigma);
	}
	// Decrease blur sigma
	if (KeyHit(Key_Subtract))
	{
		float newSigma = static_cast<CBlurEffect*>(myEffects[eBlur])->GetSigma() - 0.5f;
		if (newSigma < 0.5f)
		{
			newSigma = 0.5f;
		}
		static_cast<CBlurEffect*>(myEffects[eBlur])->ChangeSigma(newSigma);
	}

	// Increase blur kernel size
	if (KeyHit(Key_X))
	{
		int newSize = static_cast<CBlurEffect*>(myEffects[eBlur])->GetKernelSize() + 2;
		if (newSize > 15)
		{
			newSize = 15;
		}
		static_cast<CBlurEffect*>(myEffects[eBlur])->ChangeKernelSize(newSize);
	}
	// Decrease blur kernel size
	if (KeyHit(Key_Z))
	{
		int newSize = static_cast<CBlurEffect*>(myEffects[eBlur])->GetKernelSize() - 2;
		if (newSize < 1)
		{
			newSize = 1;
		}
		static_cast<CBlurEffect*>(myEffects[eBlur])->ChangeKernelSize(newSize);
	}


	// If the light is moving, stop it & vice versa
	if (KeyHit(Key_0))
	{
		lightMove = !lightMove;
	}


	// Allow the user to click on an opening in the walls etc to select it
	if (KeyHit(Mouse_LButton))
	{
		CVector3 pos;

		CEntity* wall = NULL;

		float halfWidth = 10.0f;
		float halfHeight = 10.0f;

		EntityManager.BeginEnumEntities("", "Wall1");
		wall = EntityManager.EnumEntity();
		while (wall)
		{
			pos = wall->Position();
			pos.y += 20.0f;

			SetArea(pos, 20.0f, 20.0f);
				
			wall = EntityManager.EnumEntity();
		}

		EntityManager.EndEnumEntities();



		EntityManager.BeginEnumEntities("", "Wall2");
		wall = EntityManager.EnumEntity();
		while (wall)
		{
			for (unsigned int holeNum = 0; holeNum < 4; holeNum++)
			{
				pos = wall->Position();
				pos.y += 20.0f;
				pos.x -= 36.0f;
				pos.x += 24.0f * holeNum;

				SetArea(pos, 20.0f, 20.0f);
			}

			wall = EntityManager.EnumEntity();
		}

		EntityManager.EndEnumEntities();
	}

	// Rotate cube and attach light to it
	CEntity* cubey = EntityManager.GetEntity( "Cubey" );
	if (lightMove)
	{
		cubey->Matrix().RotateX(ToRadians(53.0f) * updateTime);
		cubey->Matrix().RotateZ(ToRadians(42.0f) * updateTime);
		cubey->Matrix().RotateWorldY(ToRadians(12.0f) * updateTime);
	}
	Lights[1]->SetPosition( cubey->Position() );
	
	// Move the camera
	MainCamera->Control( Key_Up, Key_Down, Key_Left, Key_Right, Key_W, Key_S, Key_A, Key_D, 
	                     CameraMoveSpeed * updateTime, CameraRotSpeed * updateTime );

	// Accumulate update times to calculate the average over a given period
	SumUpdateTimes += updateTime;
	++NumUpdateTimes;
	if (SumUpdateTimes >= UpdateTimePeriod)
	{
		AverageUpdateTime = SumUpdateTimes / NumUpdateTimes;
		SumUpdateTimes = 0.0f;
		NumUpdateTimes = 0;
	}
}

bool SetArea(CVector3 pos, float width, float height)
{
	CVector2 pixelTL;
	CVector2 pixelTR;
	CVector2 pixelBL;
	CVector2 pixelBR;

	
	CVector3 holeTL;
	CVector3 holeTR;
	CVector3 holeBL;
	CVector3 holeBR;

	float halfWidth = width / 2.0f;
	float halfHeight = height / 2.0f;

	holeTL = pos;
	holeTR = pos;
	holeBL = pos;
	holeBR = pos;

	holeTL.x -= halfWidth;
	holeTL.y += halfHeight;

	holeTR.x += halfWidth;
	holeTR.y += halfHeight;

	holeBL.x -= halfWidth;
	holeBL.y -= halfHeight;

	holeBR.x += halfWidth;
	holeBR.y -= halfHeight;

	MainCamera->PixelFromWorldPt(&pixelTL, holeTL, BackBufferWidth, BackBufferHeight);
	MainCamera->PixelFromWorldPt(&pixelTR, holeTR, BackBufferWidth, BackBufferHeight);
	MainCamera->PixelFromWorldPt(&pixelBL, holeBL, BackBufferWidth, BackBufferHeight);
	MainCamera->PixelFromWorldPt(&pixelBR, holeBR, BackBufferWidth, BackBufferHeight);

	if ((MouseX > pixelTL.x) &&
		(MouseX > pixelBL.x) &&
		(MouseX < pixelTR.x) &&
		(MouseX < pixelBR.x) &&
		(MouseY > pixelTL.y) &&
		(MouseY > pixelTR.y) &&
		(MouseY < pixelBL.y) &&
		(MouseY < pixelBR.y))
	{
		AreaEffect = true;
		EffectArea = pos;
		return true;
	}
	return false;
}

} // namespace gen

