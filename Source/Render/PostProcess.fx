//--------------------------------------------------------------------------------------
//	File: PostProcessArea.fx
//
//	Full screen post processing
//--------------------------------------------------------------------------------------

//--------------------------------------------------------------------------------------
// Global Variables
//--------------------------------------------------------------------------------------

// Texture maps
Texture2D SceneTexture;   // Texture containing the scene to copy to the full screen quad
Texture2D BlurredTexture;	// Texture containing a blurred copy of the scene
Texture2D DepthTexture;

// Samplers to use with the above texture maps. Specifies texture filtering and addressing mode to use when accessing texture pixels
// Usually use point sampling for the scene texture (i.e. no bilinear/trilinear blending) since don't want to blur it in the copy process
SamplerState PointClamp
{
    Filter = MIN_MAG_MIP_POINT;
    AddressU = Clamp;
    AddressV = Clamp;
	MaxLOD = 0.0f;
};

// See comment above. However, screen distortions may benefit slightly from bilinear filtering (not tri-linear because we won't create mip-maps for the scene each frame)
SamplerState BilinearClamp
{
    Filter = MIN_MAG_LINEAR_MIP_POINT;
    AddressU = Clamp;
    AddressV = Clamp;
	MaxLOD = 0.0f;
};

// Use other filtering methods for the special purpose post-processing textures (e.g. the noise map)
SamplerState BilinearWrap
{
    Filter = MIN_MAG_LINEAR_MIP_POINT;
    AddressU = Wrap;
    AddressV = Wrap;
};

SamplerState TrilinearWrap
{
    Filter = MIN_MAG_MIP_LINEAR;
    AddressU = Wrap;
    AddressV = Wrap;
};


//******************************************
// Post Process Area - Dimensions

// Depth stored in .z
float3 PPAreaTopLeft;     // Top-left and bottom-right coordinates of area to post process, provided as UVs into the scene texture...
float3 PPAreaTopRight;
float3 PPAreaBottomLeft;
float3 PPAreaBottomRight; // ... i.e. the X and Y coordinates range from 0.0 to 1.0 from left->right and top->bottom of viewport

//******************************************

// Other variables used for individual post-processes
float3  TintColour;
float   BlurWeight[16];
int     BlurWidth;
float   ShakeAmount;
float   ShakeXMove;
float   ShakeYMove;

float	NearClip;
float	FarClip;
float	DoFDistance;
float	DoFRange;

// Has to be a constant array for sample to use it
static const int PixelOffset[8] = 
{
	0,
	1,
	2,
	3,
	4,
	5,
	6,
	7
};

//--------------------------------------------------------------------------------------
// Structures
//--------------------------------------------------------------------------------------

// This vertex shader input uses a special input type, the vertex ID. This value is automatically generated and does not come from a vertex buffer.
// The value starts at 0 and increases by one with each vertex processed. As this is the only input for post processing - **no vertex/index buffers are required**
struct VS_POSTPROCESS_INPUT
{
    uint vertexId : SV_VertexID;
};

// Vertex shader output / pixel shader input for the post processing shaders
// Provides the viewport positions of the quad to be post processed, then *two* UVs. The Scene UVs indicate which part of the 
// scene texture is being post-processed. The Area UVs range from 0->1 within the area only - these UVs can be used to apply a
// second texture to the area itself, or to find the location of a pixel within the area affected (the Scene UVs could be
// used together with the dimensions variables above to calculate this 2nd set of UVs, but this way saves pixel shader work)
struct PS_POSTPROCESS_INPUT
{
    float4 ProjPos : SV_POSITION;
	float2 UVScene : TEXCOORD0;
	float2 UVArea  : TEXCOORD1;
};


//--------------------------------------------------------------------------------------
// Vertex Shaders
//--------------------------------------------------------------------------------------

//******************************************
// Post Process Area - Generate Vertices

// This rather unusual shader generates its own vertices - the input data is merely the vertex ID - an automatically generated increasing index.
// No vertex or index buffer required, so convenient on the C++ side. Probably not so efficient, but fine for just a few post-processing quads
PS_POSTPROCESS_INPUT PPQuad(VS_POSTPROCESS_INPUT vIn)
{
    PS_POSTPROCESS_INPUT vOut;
	
	// The four points of a full-screen quad - will use post process area dimensions (provided above) to scale these to the correct quad needed
	float2 Quad[4] =  { float2(0.0, 0.0),   // Top-left
	                    float2(1.0, 0.0),   // Top-right
	                    float2(0.0, 1.0),   // Bottom-left
	                    float2(1.0, 1.0) }; // Bottom-right

	//***TODO - Complete each of the sections below. As usual with shaders there is much detail compressed into little code, read the comments to help

	// vOut.UVArea contains UVs for the area itself: (0,0) at top-left of area, (1,1) at bottom right. Simply the values stored in the Quad array above.
	//   So just index that array with the vertex ID (0 to 3) found inside the input structure vIn
	vOut.UVArea = Quad[vIn.vertexId];


	// vOut.UVScene contains UVs for the section of the scene texture to use. The top-left and bottom-right coordinates are already provided in
	// the PPAreaTopLeft and PPAreaBottomRight variables one pages above, but not the other two corners...
	//   Notice that where the Quad array contains a 0.0f, we want the value from the PPAreaTopLeft, and where it contains a 1.0f, we want a value
	//   from PPAreaBottomRight. This is a use for lerp: use a lerp between the values PPAreaTopLeft and PPAreaBottomRight, based on the values
	//   from the Quad array. This may seem strange, but provides flexibility later...
	//   I will go over this in the lab as necessary (or look at next week's lab for solution when available)
	vOut.UVScene = lerp(float2(PPAreaTopLeft.xy), float2(PPAreaBottomRight.xy), Quad[vIn.vertexId]);
	             
	// vOut.ProjPos contains the vertex positions of the quad to render, measured in viewport space here. This is a float4, so set x, y, z & w:
	//   The x and y coordinates are the same as the UVScene coordinates just calculated on the line above, *except* the range is
	//   from -1.0 to 1.0 rather than 0.0 to 1.0. So multiply the UVScene x & y by 2 and subtract 1
	//   Afterwards the y axis must be negated since UV space y-axis is down and viewport space y-axis is up
	//   The z value is the depth buffer value provided in the PPAreaDepth variable
	//   The w value must be 1.0f to avoid doing a perspective divide (did that already in the C++)

	vOut.ProjPos.x = (vOut.UVScene.x * 2.0f) - 1.0f;
	vOut.ProjPos.y = -((vOut.UVScene.y * 2.0f) - 1.0f);
	vOut.ProjPos.z = PPAreaTopLeft.z;
	vOut.ProjPos.w = 1.0f;
	
    return vOut;
}

//******************************************

PS_POSTPROCESS_INPUT PPQuadWorldArea(VS_POSTPROCESS_INPUT vIn)
{
	PS_POSTPROCESS_INPUT vOut;

	// The four points of a full-screen quad - will use post process area dimensions (provided above) to scale these to the correct quad needed
	float2 Quad[4] = { float2(0.0, 0.0),   // Top-left
		float2(1.0, 0.0),   // Top-right
		float2(0.0, 1.0),   // Bottom-left
		float2(1.0, 1.0) }; // Bottom-right

	float3 QuadUVs[4] = {
		PPAreaTopLeft,
		PPAreaTopRight,
		PPAreaBottomLeft,
		PPAreaBottomRight
	};

	// vOut.UVArea contains UVs for the area itself: (0,0) at top-left of area, (1,1) at bottom right. Simply the values stored in the Quad array above.
	//   So just index that array with the vertex ID (0 to 3) found inside the input structure vIn
	vOut.UVArea = Quad[vIn.vertexId];


	// vOut.UVScene contains UVs for the section of the scene texture to use. The top-left and bottom-right coordinates are already provided in
	// the PPAreaTopLeft and PPAreaBottomRight variables one pages above, but not the other two corners...
	//   Notice that where the Quad array contains a 0.0f, we want the value from the PPAreaTopLeft, and where it contains a 1.0f, we want a value
	//   from PPAreaBottomRight. This is a use for lerp: use a lerp between the values PPAreaTopLeft and PPAreaBottomRight, based on the values
	//   from the Quad array. This may seem strange, but provides flexibility later...
	//   I will go over this in the lab as necessary (or look at next week's lab for solution when available)
	vOut.UVScene = QuadUVs[vIn.vertexId].xy;

	// vOut.ProjPos contains the vertex positions of the quad to render, measured in viewport space here. This is a float4, so set x, y, z & w:
	//   The x and y coordinates are the same as the UVScene coordinates just calculated on the line above, *except* the range is
	//   from -1.0 to 1.0 rather than 0.0 to 1.0. So multiply the UVScene x & y by 2 and subtract 1
	//   Afterwards the y axis must be negated since UV space y-axis is down and viewport space y-axis is up
	//   The z value is the depth buffer value provided in the PPAreaDepth variable
	//   The w value must be 1.0f to avoid doing a perspective divide (did that already in the C++)

	vOut.ProjPos.x = (vOut.UVScene.x * 2.0f) - 1.0f;
	vOut.ProjPos.y = -((vOut.UVScene.y * 2.0f) - 1.0f);
	vOut.ProjPos.z = QuadUVs[vIn.vertexId].z;
	vOut.ProjPos.w = 1.0f;

	return vOut;
}


//--------------------------------------------------------------------------------------
// Post-processing Pixel Shaders
//--------------------------------------------------------------------------------------

// Post-processing shader that simply outputs the scene texture, i.e. no post-processing. A waste of processing, but illustrative
float4 PPCopyShader( PS_POSTPROCESS_INPUT ppIn ) : SV_Target
{
	float3 ppColour = SceneTexture.Sample(PointClamp, ppIn.UVScene).xyz;

	return float4(ppColour, 1.0f);
}


// Post-processing shader that tints the scene texture to a given colour
float4 PPTintShader( PS_POSTPROCESS_INPUT ppIn ) : SV_Target
{
	// Sample the texture colour (look at shader above) and multiply it with the tint colour (variables near top)
	float3 ppColour = SceneTexture.Sample( PointClamp, ppIn.UVScene ).xyz * TintColour;
	return float4(ppColour, 1.0f);
}


// The 2 pass blur shaders
// Both use the Blurweights and the pixel offset array to create a gaussian blur
// Call one then the other to do a full gaussian blur
float4 PPBlurXShader(PS_POSTPROCESS_INPUT ppIn) : SV_Target
{
	float3 ppColour = float3(0.0f, 0.0f, 0.0f);

	ppColour += SceneTexture.Sample(PointClamp, ppIn.UVScene, int2(0, 0)) * BlurWeight[0];
	for (int i = 1; i < BlurWidth; i++)
	{
		ppColour += SceneTexture.Sample(PointClamp, ppIn.UVScene, int2(PixelOffset[i], 0)) * BlurWeight[i];
		ppColour += SceneTexture.Sample(PointClamp, ppIn.UVScene, int2(-PixelOffset[i], 0)) * BlurWeight[i];
	}

	return float4(ppColour, 1.0f);
}

float4 PPBlurYShader(PS_POSTPROCESS_INPUT ppIn) : SV_Target
{
	float3 ppColour = float3(0.0f, 0.0f, 0.0f);

	ppColour += SceneTexture.Sample(PointClamp, ppIn.UVScene, int2(0, 0)) * BlurWeight[0];
	for (int i = 1; i < BlurWidth; i++)
	{
		ppColour += SceneTexture.Sample(PointClamp, ppIn.UVScene, int2(0, PixelOffset[i])) * BlurWeight[i];
		ppColour += SceneTexture.Sample(PointClamp, ppIn.UVScene, int2(0, -PixelOffset[i])) * BlurWeight[i];
	}

	return float4(ppColour, 1.0f);
}

// The shake effect, just shakes and moves the area, discards anything out of bounds
float4 PPShakeShader(PS_POSTPROCESS_INPUT ppIn) : SV_Target
{
	float3 ppColour = float3(0.0f, 0.0f, 0.0f);

	float2 UV = ppIn.UVScene;
	UV.x += sin(UV.y * 20) * ShakeAmount;

	UV.x += ShakeXMove;
	UV.y += ShakeYMove;

	if ((UV.x < 0.0f) || (UV.x > 1.0f))
	{
		discard;
	}
	if ((UV.y < 0.0f) || (UV.y > 1.0f))
	{
		discard;
	}

	ppColour = SceneTexture.Sample(PointClamp, UV);

	return float4(ppColour, 1.0f);
}

// Returns the inverse of the current colour
float4 PPNegativeShader(PS_POSTPROCESS_INPUT ppIn) : SV_Target
{
	float3 ppColour = SceneTexture.Sample(PointClamp, ppIn.UVScene).xyz;

	ppColour.x = 1.0f - ppColour.x;
	ppColour.y = 1.0f - ppColour.y;
	ppColour.z = 1.0f - ppColour.z;

	return float4(ppColour, 1.0f);
}


// Uses microsoft values for sepia toning pictures
float4 PPSepiaShader(PS_POSTPROCESS_INPUT ppIn) : SV_Target
{
	float3 inColour = SceneTexture.Sample(PointClamp, ppIn.UVScene).xyz;

	float3 ppColour;

	ppColour.r = saturate((inColour.r * 0.393f) + (inColour.g * 0.769f) + (inColour.b * 0.189f));
	ppColour.g = saturate((inColour.r * 0.349f) + (inColour.g * 0.686f) + (inColour.b * 0.168f));
	ppColour.b = saturate((inColour.r * 0.272f) + (inColour.g * 0.534f) + (inColour.b * 0.131f));

	return float4(ppColour, 1.0f);
}

// Returns the average of the input colours for each channel
float4 PPGrayscaleShader(PS_POSTPROCESS_INPUT ppIn) : SV_Target
{
	float3 inColour = SceneTexture.Sample(PointClamp, ppIn.UVScene).xyz;

	float outColour = (inColour.r + inColour.g + inColour.b) / 3.0f;

	float3 ppColour = float3(outColour, outColour, outColour);

	return float4(ppColour, 1.0f);
}

// Uses the non-blurred render for the DoF distance and lerps accross the range outwards into a blurred scene.
// Based on algorithm found here https://digitalerr0r.wordpress.com/2009/05/16/xna-shader-programming-tutorial-20-depth-of-field/
float4 PPDoFShader(PS_POSTPROCESS_INPUT ppIn) : SV_Target
{
	float3 ppColour;
	float3 scene = SceneTexture.Sample(PointClamp, ppIn.UVScene).xyz;
	float3 blurScene = BlurredTexture.Sample(PointClamp, ppIn.UVScene).xyz;
	float  depth = DepthTexture.Sample(PointClamp, ppIn.UVScene).r;

	float modFarClip = FarClip / (FarClip - NearClip);

	float sceneZ = (-NearClip * modFarClip) / (depth - modFarClip);
	float blur = saturate(abs(sceneZ - DoFDistance) / DoFRange);

	ppColour = lerp(scene, blurScene, blur);

	return float4(ppColour, 1.0f);
}

// Not a very good implementation - needs work
// Lerps into the blurred scene if one channel is above a threshold, based on the blurred scene to start (so the blur looks to bleed into non-blurred)
float4 PPBloomShader(PS_POSTPROCESS_INPUT ppIn) : SV_Target
{
	float3 ppColour;
	float3 scene = SceneTexture.Sample(PointClamp, ppIn.UVScene).xyz;
	float3 blurScene = BlurredTexture.Sample(PointClamp, ppIn.UVScene).xyz;
	float blur = 0.0f;
	float blurThreshold = 0.8f;
	float totalColour = max(max(blurScene.x, blurScene.y), blurScene.z);
	if (totalColour > blurThreshold)
	{
		blur = lerp(blurThreshold, 1.0f, ((totalColour - blurThreshold) * (1.0f / blurThreshold)));
	}

	ppColour = lerp(scene, blurScene, blur);

	return float4(ppColour, 1.0f);
}

//--------------------------------------------------------------------------------------
// States
//--------------------------------------------------------------------------------------

RasterizerState CullBack  // Cull back facing polygons - post-processing quads should be oriented facing the camera
{
	CullMode = Back;
};
RasterizerState CullNone  // Cull none of the polygons, i.e. show both sides
{
	CullMode = None;
};

DepthStencilState DepthWritesOn  // Write to the depth buffer - normal behaviour 
{
	DepthWriteMask = ALL;
};
DepthStencilState DepthWritesOff // Don't write to the depth buffer, but do read from it - useful for area based post-processing. Full screen post-process is given 0 depth, area post-processes
{                                // given a valid depth in the scene. Post-processes will not obsucre each other (in particular full-screen will not obscure area), but sorting issues may remain
	DepthWriteMask = ZERO;
};
DepthStencilState DisableDepth   // Disable depth buffer entirely
{
	DepthFunc      = ALWAYS;
	DepthWriteMask = ZERO;
};

BlendState NoBlending // Switch off blending - pixels will be opaque
{
    BlendEnable[0] = FALSE;
};
BlendState AlphaBlending
{
    BlendEnable[0] = TRUE;
    SrcBlend = SRC_ALPHA;
    DestBlend = INV_SRC_ALPHA;
    BlendOp = ADD;
};
BlendState AddBlending
{
	BlendEnable[0] = TRUE;
	SrcBlend = ONE;
	DestBlend = ONE;
	BlendOp = ADD;
};
BlendState MultBlending
{
	BlendEnable[0] = TRUE;
	SrcBlend = ZERO;
	DestBlend = SRC_COLOR;
	BlendOp = ADD;
};


//--------------------------------------------------------------------------------------
// Post Processing Techniques
//--------------------------------------------------------------------------------------

// Simple copy technique - no post-processing (pointless but illustrative)
technique10 PPCopy
{
    pass P0
    {
        SetVertexShader( CompileShader( vs_4_0, PPQuad() ) );
        SetGeometryShader( NULL );                                   
        SetPixelShader( CompileShader( ps_4_0, PPCopyShader() ) );

		SetBlendState(NoBlending, float4(0.0f, 0.0f, 0.0f, 0.0f), 0xFFFFFFFF);
		SetRasterizerState( CullBack ); 
		SetDepthStencilState( DepthWritesOff, 0 );
     }
}

technique10 PPCopyWorldArea
{
	pass P0
	{
		SetVertexShader(CompileShader(vs_4_0, PPQuadWorldArea()));
		SetGeometryShader(NULL);
		SetPixelShader(CompileShader(ps_4_0, PPCopyShader()));

		SetBlendState(NoBlending, float4(0.0f, 0.0f, 0.0f, 0.0f), 0xFFFFFFFF);
		SetRasterizerState(CullBack);
		SetDepthStencilState(DepthWritesOff, 0);
	}
}

// Tint the scene to a colour
technique10 PPTint
{
    pass P0
    {
        SetVertexShader( CompileShader( vs_4_0, PPQuad() ) );
        SetGeometryShader( NULL );                                   
        SetPixelShader( CompileShader( ps_4_0, PPTintShader() ) );

		SetBlendState( NoBlending, float4( 0.0f, 0.0f, 0.0f, 0.0f ), 0xFFFFFFFF );
		SetRasterizerState( CullBack ); 
		SetDepthStencilState(DepthWritesOff, 0);
     }
}

// Use a 2 pass gaussian blur
technique10 PPBlur
{
	pass P0
	{
		SetVertexShader(CompileShader(vs_4_0, PPQuad()));
		SetGeometryShader(NULL);
		SetPixelShader(CompileShader(ps_4_0, PPBlurXShader()));

		SetBlendState(NoBlending, float4(0.0f, 0.0f, 0.0f, 0.0f), 0xFFFFFFFF);
		SetRasterizerState(CullNone);
		SetDepthStencilState(DepthWritesOff, 0);
	}
	pass P1
	{
		SetVertexShader(CompileShader(vs_4_0, PPQuad()));
		SetGeometryShader(NULL);
		SetPixelShader(CompileShader(ps_4_0, PPBlurYShader()));

		SetBlendState(NoBlending, float4(0.0f, 0.0f, 0.0f, 0.0f), 0xFFFFFFFF);
		SetRasterizerState(CullNone);
		SetDepthStencilState(DepthWritesOff, 0);
	}
}

// Shakes the screen
technique10 PPShake
{
	pass P0
	{
		SetVertexShader(CompileShader(vs_4_0, PPQuad()));
		SetGeometryShader(NULL);
		SetPixelShader(CompileShader(ps_4_0, PPShakeShader()));

		SetBlendState(NoBlending, float4(0.0f, 0.0f, 0.0f, 0.0f), 0xFFFFFFFF);
		SetRasterizerState(CullBack);
		SetDepthStencilState(DepthWritesOff, 0);
	}
}

// Negates the colours output
technique10 PPNegativeColour
{
	pass P0
	{
		SetVertexShader(CompileShader(vs_4_0, PPQuad()));
		SetGeometryShader(NULL);
		SetPixelShader(CompileShader(ps_4_0, PPNegativeShader()));

		SetBlendState(NoBlending, float4(0.0f, 0.0f, 0.0f, 0.0f), 0xFFFFFFFF);
		SetRasterizerState(CullBack);
		SetDepthStencilState(DepthWritesOff, 0);
	}
}

// Creates a sepia toned scene
technique10 PPSepia
{
	pass P0
	{
		SetVertexShader(CompileShader(vs_4_0, PPQuad()));
		SetGeometryShader(NULL);
		SetPixelShader(CompileShader(ps_4_0, PPSepiaShader()));

		SetBlendState(NoBlending, float4(0.0f, 0.0f, 0.0f, 0.0f), 0xFFFFFFFF);
		SetRasterizerState(CullBack);
		SetDepthStencilState(DepthWritesOff, 0);
	}
}

// Averages the pixels channels to create a gray-scape
technique10 PPGrayscale
{
	pass P0
	{
		SetVertexShader(CompileShader(vs_4_0, PPQuad()));
		SetGeometryShader(NULL);
		SetPixelShader(CompileShader(ps_4_0, PPGrayscaleShader()));

		SetBlendState(NoBlending, float4(0.0f, 0.0f, 0.0f, 0.0f), 0xFFFFFFFF);
		SetRasterizerState(CullBack);
		SetDepthStencilState(DepthWritesOff, 0);
	}
}

// Treats one distance as "clean" and blurs outwards
technique10 PPDoF
{
	pass P0
	{
		SetVertexShader(CompileShader(vs_4_0, PPQuad()));
		SetGeometryShader(NULL);
		SetPixelShader(CompileShader(ps_4_0, PPDoFShader()));

		SetBlendState(NoBlending, float4(0.0f, 0.0f, 0.0f, 0.0f), 0xFFFFFFFF);
		SetRasterizerState(CullBack);
		SetDepthStencilState(DisableDepth, 0);
	}
}

// Uses blurred values to find where the area is bright (or has strong colour)
technique10 PPBloom
{
	pass P0
	{
		SetVertexShader(CompileShader(vs_4_0, PPQuad()));
		SetGeometryShader(NULL);
		SetPixelShader(CompileShader(ps_4_0, PPBloomShader()));

		SetBlendState(NoBlending, float4(0.0f, 0.0f, 0.0f, 0.0f), 0xFFFFFFFF);
		SetRasterizerState(CullBack);
		SetDepthStencilState(DisableDepth, 0);
	}
}