#ifndef __DOFEFFECTH
#define __DOFEFFECTH

#include "BaseEffect.h"
#include "BlurEffect.h"	// Needs to be able to blur

class CDoFEffect : public CBaseEffect
{
public:
	CDoFEffect(std::string PPTechName, std::string TechName);
	~CDoFEffect();
	bool SetVariables();
	bool Update(float updateTime);
	bool Render(unsigned int pass);
	bool Release();
	bool SetDoFDistance(float DoFDistance);
	float GetDoFDistance();
	bool SetDoFRange(float DoFRange);
	float GetDoFRange();
	static bool CreateTexture(unsigned int width, unsigned int height);
	static bool SetBlurEffect(CBlurEffect* blurEffect);

	static bool	SetVariables(
		ID3D10EffectShaderResourceVariable* BlurTextureVar,
		ID3D10EffectShaderResourceVariable* DepthTextureVar,
		ID3D10EffectScalarVariable* NearClipVar,
		ID3D10EffectScalarVariable* FarClipVar,
		ID3D10EffectScalarVariable* DoFDistanceVar,
		ID3D10EffectScalarVariable* DoFRangeVar);

	static bool SetDepthStencilResource(ID3D10ShaderResourceView* m_DepthStencilResource);
	static bool SetNearFarClip(float NearClip, float FarClip);
	std::string OutInfo();
private:
	CDoFEffect();
	CDoFEffect(const CDoFEffect&);
	CDoFEffect& operator= (const CDoFEffect&);

	static ID3D10EffectShaderResourceVariable*  m_BlurTextureVar;
	static ID3D10Texture2D*						m_BlurTexture;
	static ID3D10RenderTargetView*				m_BlurRenderTarget;
	static ID3D10ShaderResourceView*			m_BlurShaderResource;

	static ID3D10EffectShaderResourceVariable*  m_DepthTextureVar;
	static ID3D10ShaderResourceView*			m_DepthStencilResource;

	static CBlurEffect*							m_BlurEffect;

	static ID3D10EffectScalarVariable*			m_NearClipVar;
	static ID3D10EffectScalarVariable*			m_FarClipVar;
	static ID3D10EffectScalarVariable*			m_DoFDistanceVar;
	static ID3D10EffectScalarVariable*			m_DoFRangeVar;

	static float m_NearClip;
	static float m_FarClip;
	float m_DoFDistance;
	float m_DoFRange;
};

#endif