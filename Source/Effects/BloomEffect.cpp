#include "BloomEffect.h"

CBloomEffect::CBloomEffect(std::string PPTechName, std::string TechName) : CBaseEffect(PPTechName, TechName, eBloom, 1)
{

}

CBloomEffect::~CBloomEffect()
{

}

bool CBloomEffect::Release()
{
	if (m_BlurShaderResource)	m_BlurShaderResource->Release();
	if (m_BlurRenderTarget)		m_BlurRenderTarget->Release();
	if (m_BlurTexture)			m_BlurTexture->Release();
	return true;
}

bool CBloomEffect::SetVariables(
	ID3D10EffectShaderResourceVariable* BlurTextureVar,
	ID3D10EffectShaderResourceVariable* DepthTextureVar)
{
	m_BlurTextureVar = BlurTextureVar;
	m_DepthTextureVar = DepthTextureVar;
	return true;
}


bool CBloomEffect::SetDepthStencilResource(ID3D10ShaderResourceView* DepthStencilResource)
{
	m_DepthStencilResource = DepthStencilResource;
	return true;
}

ID3D10EffectShaderResourceVariable* CBloomEffect::m_BlurTextureVar(NULL);
CBlurEffect*						CBloomEffect::m_BlurEffect(NULL);
ID3D10ShaderResourceView*			CBloomEffect::m_DepthStencilResource(NULL);
ID3D10EffectShaderResourceVariable* CBloomEffect::m_DepthTextureVar(NULL);

ID3D10Texture2D*			CBloomEffect::m_BlurTexture(NULL);
ID3D10RenderTargetView*		CBloomEffect::m_BlurRenderTarget(NULL);
ID3D10ShaderResourceView*	CBloomEffect::m_BlurShaderResource(NULL);


bool CBloomEffect::CreateTexture(unsigned int width, unsigned int height)
{
	m_BlurTexture;
	m_BlurRenderTarget;
	m_BlurShaderResource;

	D3D10_TEXTURE2D_DESC textureDesc;
	textureDesc.Width = width;  // Match views to viewport size
	textureDesc.Height = height;
	textureDesc.MipLevels = 1; // No mip-maps when rendering to textures (or we will have to render every level)
	textureDesc.ArraySize = 1;
	textureDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM; // RGBA texture (8-bits each)
	textureDesc.SampleDesc.Count = 1;
	textureDesc.SampleDesc.Quality = 0;
	textureDesc.Usage = D3D10_USAGE_DEFAULT;
	textureDesc.BindFlags = D3D10_BIND_RENDER_TARGET | D3D10_BIND_SHADER_RESOURCE; // Indicate we will use texture as render target, and pass it to shaders
	textureDesc.CPUAccessFlags = 0;
	textureDesc.MiscFlags = 0;
	if (FAILED(GetDevice()->CreateTexture2D(&textureDesc, NULL, &m_BlurTexture))) return false;

	// Get a "view" of the texture as a render target - giving us an interface for rendering to the texture
	if (FAILED(GetDevice()->CreateRenderTargetView(m_BlurTexture, NULL, &m_BlurRenderTarget))) return false;

	// And get a shader-resource "view" - giving us an interface for passing the texture to shaders
	D3D10_SHADER_RESOURCE_VIEW_DESC srDesc;
	srDesc.Format = textureDesc.Format;
	srDesc.ViewDimension = D3D10_SRV_DIMENSION_TEXTURE2D;
	srDesc.Texture2D.MostDetailedMip = 0;
	srDesc.Texture2D.MipLevels = 1;
	if (FAILED(GetDevice()->CreateShaderResourceView(m_BlurTexture, &srDesc, &m_BlurShaderResource))) return false;

	return true;
}

bool CBloomEffect::SetBlurEffect(CBlurEffect* blurEffect)
{
	m_BlurEffect = blurEffect;
	return true;
}

std::string CBloomEffect::OutInfo()
{
	std::stringstream sstream;
	return sstream.str();
}


bool CBloomEffect::SetVariables()
{
	return true;
}

bool CBloomEffect::Update(float updateTime)
{
	return true;
}

bool CBloomEffect::Render(unsigned int pass)
{
	ID3D10RenderTargetView* target;
	ID3D10ShaderResourceView* resource;
	ID3D10ShaderResourceView *const pSRV[1] = { NULL };
	GetDevice()->PSSetShaderResources(0, 1, pSRV);
	GetDevice()->PSSetShaderResources(1, 1, pSRV);
	GetDevice()->PSSetShaderResources(2, 1, pSRV);

	m_BlurEffect->SetVariables();

	// Render the blurred scene first pass to one scene texture
	if (CurrentTarget() == 1)
	{
		resource = GetSceneShaderResource2();
		target = GetSceneRenderTarget1();
	}
	else
	{
		resource = GetSceneShaderResource1();
		target = GetSceneRenderTarget2();
	}


	GetSceneTextureVar()->SetResource(resource);

	GetDevice()->OMSetRenderTargets(1, &target, GetDepthStencilView());

	m_BlurEffect->DrawPass(0);

	CBaseEffect::SwitchCurrentTarget();
	
	// Clear shader resources
	GetDevice()->PSSetShaderResources(0, 1, pSRV);
	GetDevice()->PSSetShaderResources(1, 1, pSRV);
	GetDevice()->PSSetShaderResources(2, 1, pSRV);
	// Render the blurred scene second pass to the blur texture
	// This is so we don't overwrite the original texture for the unblurred scene
	if (CurrentTarget() == 1)
	{
		resource = GetSceneShaderResource2();
	}
	else
	{
		resource = GetSceneShaderResource1();
	}

	GetSceneTextureVar()->SetResource(resource);

	GetDevice()->OMSetRenderTargets(1, &m_BlurRenderTarget, GetDepthStencilView());

	m_BlurEffect->DrawPass(1);
	
	// Clear shader resources
	GetDevice()->PSSetShaderResources(0, 1, pSRV);
	GetDevice()->PSSetShaderResources(1, 1, pSRV);
	GetDevice()->PSSetShaderResources(2, 1, pSRV);

	// Render the pass with DoF
	// Set unblurred scene resource and other scene texture as target.
	// Set blurred texture

	if (CurrentTarget() == 1)
	{
		resource = GetSceneShaderResource1();
		target = GetSceneRenderTarget2();
	}
	else
	{
		resource = GetSceneShaderResource2();
		target = GetSceneRenderTarget1();
	}
	// Set blurred resource
	GetDevice()->OMSetRenderTargets(1, &target, NULL);


	m_DepthTextureVar->SetResource(m_DepthStencilResource);
	GetSceneTextureVar()->SetResource(resource);
	m_BlurTextureVar->SetResource(m_BlurShaderResource);

	GetEffectTechnique()->GetPassByIndex(0)->Apply(0);

	GetDevice()->Draw(4, 0);

	GetDevice()->PSSetShaderResources(0, 1, pSRV);
	GetDevice()->PSSetShaderResources(1, 1, pSRV);
	GetDevice()->PSSetShaderResources(2, 1, pSRV);

	CBaseEffect::SwitchCurrentTarget();

	return true;
}