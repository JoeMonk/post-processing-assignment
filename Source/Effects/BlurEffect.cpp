#include "BlurEffect.h"

CBlurEffect::CBlurEffect(std::string PPTechName, std::string TechName) : CBaseEffect(PPTechName, TechName, eBlur, 2)
{
	m_GaussianSigma = 2.0f;
	m_GaussianKernelSize = 7;	// Has to be odd
	m_BlurWeightVar = NULL;
	m_BlurWidthVar = NULL;
}

CBlurEffect::~CBlurEffect()
{
	
}


ID3D10EffectScalarVariable* CBlurEffect::m_BlurWeightVar(NULL);
ID3D10EffectScalarVariable* CBlurEffect::m_BlurWidthVar(NULL);

bool CBlurEffect::Release()
{
	return true;
}

bool CBlurEffect::LoadVariables(ID3D10EffectScalarVariable* BlurWeightVar, ID3D10EffectScalarVariable* BlurWidthVar)
{
	m_BlurWeightVar = BlurWeightVar;
	m_BlurWidthVar = BlurWidthVar;
	return true;
}

bool CBlurEffect::ChangeSigma(float sigma)
{
	m_GaussianSigma = sigma;
	return true;
}

float CBlurEffect::GetSigma()
{
	return m_GaussianSigma;
}

bool CBlurEffect::ChangeKernelSize(int kernelSize)
{
	if (kernelSize % 2 == 1)
	{
		m_GaussianKernelSize = kernelSize;
		return true;
	}
	return false;
}

int CBlurEffect::GetKernelSize()
{
	return m_GaussianKernelSize;
}

std::string CBlurEffect::OutInfo()
{
	std::stringstream sstream;
	sstream.precision(1);
	sstream << std::fixed;
	sstream << ": Sigma = " << m_GaussianSigma;
	sstream << std::endl;
	sstream << ": Kernel Size = " << m_GaussianKernelSize;
	return std::string(sstream.str());
}

bool CBlurEffect::SetVariables()
{
	float kernel[16];
	int mean = m_GaussianKernelSize / 2;
	int halfway = mean + 1;
	float sum = 0.0f; // For accumulating the kernel values
	for (int x = 0; x < m_GaussianKernelSize; x++)
	{
		kernel[x] = expf(-0.5f * (powf((x - mean) / m_GaussianSigma, 2.0f)));

		// Accumulate the kernel values
		sum += kernel[x];
	}

	// Normalize the kernel
	for (int x = 0; x <= m_GaussianKernelSize; x++)
	{
		kernel[x] /= sum;
	}

	float oneSide[8];
	for (int i = 0; i < halfway; i++)
	{
		oneSide[i] = kernel[mean + i];
	}

	m_BlurWeightVar->SetFloatArray(oneSide, 0, halfway);
	m_BlurWidthVar->SetInt(halfway);

	return true;
}

bool CBlurEffect::Update(float updateTime)
{
	return true;
}

bool CBlurEffect::Render(unsigned int pass)
{

	ID3D10RenderTargetView* target;

	ID3D10ShaderResourceView *const pSRV[1] = { NULL };
	GetDevice()->PSSetShaderResources(0, 1, pSRV);

	if (CurrentTarget() == 1)
	{
		GetSceneTextureVar()->SetResource(GetSceneShaderResource2());
		target = GetSceneRenderTarget1();
	}
	else
	{
		GetSceneTextureVar()->SetResource(GetSceneShaderResource1());
		target = GetSceneRenderTarget2();
	}

	GetDevice()->OMSetRenderTargets(1, &target, GetDepthStencilView());


	GetEffectTechnique()->GetPassByIndex(pass)->Apply(0);
	GetDevice()->Draw(4, 0);

	return true;
}

// Override for use in other effects
bool CBlurEffect::DrawPass(unsigned int pass)
{
	GetEffectTechnique()->GetPassByIndex(pass)->Apply(0);
	GetDevice()->Draw(4, 0);

	return true;
}