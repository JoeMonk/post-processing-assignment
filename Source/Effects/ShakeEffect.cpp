#include "ShakeEffect.h"

CShakeEffect::CShakeEffect(std::string PPTechName, std::string TechName) : CBaseEffect(PPTechName, TechName, eShake, 1)
{
	m_ShakeAmountVar = NULL;

	m_CurrentShake = 0.0f;
	m_CurrentTime = 0.0f;
	m_ShakeTime = 4.0f;
	m_ShakeSpeed = 0.25f;
	m_ShakeForwards = true;
	m_MaxShakeLimit = (m_ShakeSpeed / m_ShakeTime) * 1.5f;
	m_ShakeLimit = m_MaxShakeLimit;

	m_XShake = 0.0f;
	m_YShake = 0.0f;

	m_XForwards = true;
	m_YForwards = true;

	m_MaxShakeMove = 0.05f;
	m_CurrentMaxXMove = m_MaxShakeMove;
	m_CurrentMaxYMove = m_MaxShakeMove;
	m_MoveMult = 0.95f;
}

CShakeEffect::~CShakeEffect()
{

}

ID3D10EffectScalarVariable* CShakeEffect::m_ShakeAmountVar(NULL);
ID3D10EffectScalarVariable* CShakeEffect::m_ShakeXMoveVar(NULL);
ID3D10EffectScalarVariable* CShakeEffect::m_ShakeYMoveVar(NULL);

bool CShakeEffect::Release()
{
	return true;
}

std::string CShakeEffect::OutInfo()
{
	std::stringstream sstream;
	sstream.precision(2);
	sstream << std::fixed;
	sstream << (m_ShakeTime - m_CurrentTime);
	return std::string(": Seconds left: " + sstream.str());
}

bool CShakeEffect::LoadVariables(ID3D10EffectScalarVariable* ShakeAmountVar, ID3D10EffectScalarVariable* ShakeXMoveVar, ID3D10EffectScalarVariable* ShakeYMoveVar)
{
	m_ShakeAmountVar = ShakeAmountVar;
	m_ShakeXMoveVar = ShakeXMoveVar;
	m_ShakeYMoveVar = ShakeYMoveVar;
	return true;
}

bool CShakeEffect::SetVariables()
{
	m_ShakeXMoveVar->SetFloat(m_XShake);
	m_ShakeYMoveVar->SetFloat(m_YShake);
	m_ShakeAmountVar->SetFloat(m_CurrentShake);
	return true;
}

bool CShakeEffect::Update(float updateTime)
{
	m_CurrentTime += updateTime;
	if (m_ShakeForwards)
	{
		m_CurrentShake += m_ShakeSpeed * updateTime;
		if (m_CurrentShake > m_ShakeLimit)
		{
			m_ShakeForwards = !m_ShakeForwards;
			m_ShakeLimit *= 0.8f;
		}
	}
	else
	{
		m_CurrentShake -= m_ShakeSpeed * updateTime;
		if (m_CurrentShake < -m_ShakeLimit)
		{
			m_ShakeForwards = !m_ShakeForwards;
			m_ShakeLimit *= 0.8f;
		}
	}

	if (m_XForwards)
	{
		m_XShake += (static_cast<float>(rand()) / static_cast<float>(RAND_MAX)) * updateTime;
		if (m_XShake > m_CurrentMaxXMove)
		{
			m_XShake = m_CurrentMaxXMove;
			m_XForwards = false;
			m_CurrentMaxXMove *= m_MoveMult;
		}
	}
	else
	{
		m_XShake -= (static_cast<float>(rand()) / static_cast<float>(RAND_MAX)) * updateTime;
		if (m_XShake < -m_CurrentMaxXMove)
		{
			m_XShake = -m_CurrentMaxXMove;
			m_XForwards = true;
			m_CurrentMaxXMove *= m_MoveMult;
		}
	}

	if (m_YForwards)
	{
		m_YShake += (static_cast<float>(rand()) / static_cast<float>(RAND_MAX)) * updateTime;
		if (m_YShake > m_CurrentMaxYMove)
		{
			m_YShake = m_CurrentMaxYMove;
			m_YForwards = false;
			m_CurrentMaxYMove *= m_MoveMult;
		}
	}
	else
	{
		m_YShake -= (static_cast<float>(rand()) / static_cast<float>(RAND_MAX)) * updateTime;
		if (m_YShake < -m_CurrentMaxYMove)
		{
			m_YShake = -m_CurrentMaxYMove;
			m_YForwards = true;
			m_CurrentMaxYMove *= m_MoveMult;
		}
	}

	if (m_CurrentTime >= m_ShakeTime)
	{
		for (std::vector<EPostProcesses>::iterator itr = GetTechniques()->begin(); itr != GetTechniques()->end(); ++itr)
		{
			if (*itr == eShake)
			{
				GetTechniques()->erase(itr);
				ResetShake();
				break;
			}
		}
	}

	return true;
}

bool CShakeEffect::ResetShake()
{
	m_CurrentTime = 0.0f;
	m_CurrentShake = 0.0f;
	m_ShakeLimit = m_MaxShakeLimit;

	m_XForwards = true;
	m_YForwards = true;
	m_XShake = 0.0f;
	m_YShake = 0.0f;
	m_CurrentMaxXMove = m_MaxShakeMove;
	m_CurrentMaxYMove = m_MaxShakeMove;

	return true;
}

bool CShakeEffect::Render(unsigned int pass)
{
	ID3D10RenderTargetView* target;
	ID3D10ShaderResourceView *const pSRV[1] = { NULL };
	GetDevice()->PSSetShaderResources(0, 1, pSRV);

	if (CurrentTarget() == 1)
	{
		GetSceneTextureVar()->SetResource(GetSceneShaderResource2());
		target = GetSceneRenderTarget1();
	}
	else
	{
		GetSceneTextureVar()->SetResource(GetSceneShaderResource1());
		target = GetSceneRenderTarget2();
	}
	GetDevice()->OMSetRenderTargets(1, &target, GetDepthStencilView());

	GetEffectTechnique()->GetPassByIndex(0)->Apply(0);
	GetDevice()->Draw(4, 0);

	return true;
}