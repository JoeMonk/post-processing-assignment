#include "BaseEffect.h"

CBaseEffect::CBaseEffect(std::string PPTechName, std::string TechName, EPostProcesses effect, unsigned int passes)
{
	m_PPTechName = PPTechName;
	m_TechName = TechName;
	m_Effect = effect;
	m_AreaPP = false;
	m_Passes = passes;

}

CBaseEffect::~CBaseEffect()
{

}

bool CBaseEffect::SetEffectTechnique(ID3D10EffectTechnique* effectTechnique)
{
	m_EffectTechnique = effectTechnique;
	if (m_EffectTechnique)
	{
		return true;
	}
	return false;
}

ID3D10EffectTechnique* CBaseEffect::GetEffectTechnique()
{
	return m_EffectTechnique;
}

std::vector<EPostProcesses>* CBaseEffect::m_CurrentTechniques(NULL);

bool CBaseEffect::SetTechniques(std::vector<EPostProcesses>* currentTechniques)
{
	if (m_CurrentTechniques == NULL)
	{
		m_CurrentTechniques = currentTechniques;
	}

	return true;
}

std::vector<EPostProcesses>* CBaseEffect::GetTechniques()
{
	return m_CurrentTechniques;
}

ID3D10Device* CBaseEffect::m_pd3dDevice(NULL);

bool CBaseEffect::SetDevice(ID3D10Device* g_pd3dDevice)
{
	if (m_pd3dDevice == NULL)
	{
		m_pd3dDevice = g_pd3dDevice;
	}

	return true;
}

ID3D10Device* CBaseEffect::GetDevice()
{
	return m_pd3dDevice;
}

bool CBaseEffect::SwitchProcess()
{
	if (!m_CurrentTechniques->empty())
	{
		for (std::vector<EPostProcesses>::iterator itr = m_CurrentTechniques->begin(); itr != m_CurrentTechniques->end(); ++itr)
		{
			if (*itr == m_Effect)
			{
				m_CurrentTechniques->erase(itr);
				//std::sort(m_CurrentTechniques->begin(), m_CurrentTechniques->end());
				return false;
			}
		}

		m_CurrentTechniques->push_back(m_Effect);
		//std::sort(m_CurrentTechniques->begin(), m_CurrentTechniques->end());
		return true;
	}
	else
	{
		m_CurrentTechniques->push_back(m_Effect);
		//std::sort(m_CurrentTechniques->begin(), m_CurrentTechniques->end());
		return true;
	}
}

std::string CBaseEffect::GetTechName()
{
	return m_TechName;
}

std::string CBaseEffect::GetPPTechName()
{
	return m_PPTechName;
}

bool CBaseEffect::SetSceneTextureVar(ID3D10EffectShaderResourceVariable* SceneTextureVar)
{
	m_SceneTextureVar = SceneTextureVar;
	return true;
}
bool CBaseEffect::SetSceneShaderResource1(ID3D10ShaderResourceView* SceneShaderResource1)
{
	m_SceneShaderResource1 = SceneShaderResource1;
	return true;
}
bool CBaseEffect::SetSceneShaderResource2(ID3D10ShaderResourceView* SceneShaderResource2)
{
	m_SceneShaderResource2 = SceneShaderResource2;
	return true;
}
bool CBaseEffect::SetSceneRenderTarget1(ID3D10RenderTargetView*	SceneRenderTarget1)
{
	m_SceneRenderTarget1 = SceneRenderTarget1;
	return true;
}
bool CBaseEffect::SetSceneRenderTarget2(ID3D10RenderTargetView*	SceneRenderTarget2)
{
	m_SceneRenderTarget2 = SceneRenderTarget2;
	return true;
}
bool CBaseEffect::SetDepthStencilView(ID3D10DepthStencilView* DepthStencilView)
{
	m_DepthStencilView = DepthStencilView;
	return true;
}

unsigned int CBaseEffect::m_CurrentTarget(1);

unsigned int CBaseEffect::CurrentTarget()
{
	return m_CurrentTarget;
}
bool CBaseEffect::SwitchCurrentTarget()
{
	if (CurrentTarget() == 1)
	{
		m_CurrentTarget = 2;
		return true;
	}

	m_CurrentTarget = 1;
	return true;
}


unsigned int CBaseEffect::GetPasses()
{
	return m_Passes;
}

ID3D10EffectShaderResourceVariable* CBaseEffect::m_SceneTextureVar(NULL);
ID3D10ShaderResourceView* CBaseEffect::m_SceneShaderResource1(NULL);
ID3D10ShaderResourceView* CBaseEffect::m_SceneShaderResource2(NULL);
ID3D10RenderTargetView* CBaseEffect::m_SceneRenderTarget1(NULL);
ID3D10RenderTargetView* CBaseEffect::m_SceneRenderTarget2(NULL);
ID3D10DepthStencilView*	CBaseEffect::m_DepthStencilView(NULL);

ID3D10EffectShaderResourceVariable*	CBaseEffect::GetSceneTextureVar()
{
	return m_SceneTextureVar;
}
ID3D10ShaderResourceView* CBaseEffect::GetSceneShaderResource1()
{
	return m_SceneShaderResource1;
}
ID3D10ShaderResourceView* CBaseEffect::GetSceneShaderResource2()
{
	return m_SceneShaderResource2;
}

ID3D10RenderTargetView*	CBaseEffect::GetSceneRenderTarget1()
{
	return m_SceneRenderTarget1;
}
ID3D10RenderTargetView* CBaseEffect::GetSceneRenderTarget2()
{
	return m_SceneRenderTarget2;
}
ID3D10DepthStencilView* CBaseEffect::GetDepthStencilView()
{
	return m_DepthStencilView;
}

bool CBaseEffect::GetAreaPP()
{
	return m_AreaPP;
}
void CBaseEffect::AreaPPOn()
{
	m_AreaPP = true;
}
void CBaseEffect::AreaPPOff()
{
	m_AreaPP = false;
}