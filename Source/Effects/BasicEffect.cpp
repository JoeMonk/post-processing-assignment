#include "BasicEffect.h"

CBasicEffect::CBasicEffect(std::string PPTechName, std::string TechName, EPostProcesses PostProcess) : CBaseEffect(PPTechName, TechName, PostProcess, 1)
{

}

CBasicEffect::~CBasicEffect()
{
	
}

bool CBasicEffect::Release()
{
	return true;
}

std::string CBasicEffect::OutInfo()
{
	return std::string("");
}

bool CBasicEffect::SetVariables()
{
	return true;
}

bool CBasicEffect::Update(float updateTime)
{
	return true;
}

bool CBasicEffect::Render(unsigned int pass)
{
	ID3D10RenderTargetView* target;
	ID3D10ShaderResourceView *const pSRV[1] = { NULL };
	GetDevice()->PSSetShaderResources(0, 1, pSRV);

	if (CurrentTarget() == 1)
	{
		GetSceneTextureVar()->SetResource(GetSceneShaderResource2());
		target = GetSceneRenderTarget1();
	}
	else
	{
		GetSceneTextureVar()->SetResource(GetSceneShaderResource1());
		target = GetSceneRenderTarget2();
	}
	GetDevice()->OMSetRenderTargets(1, &target, GetDepthStencilView());

	GetEffectTechnique()->GetPassByIndex(0)->Apply(0);
	GetDevice()->Draw(4, 0);

	return true;
}