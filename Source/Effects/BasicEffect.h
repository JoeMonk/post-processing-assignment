#ifndef __BASICEFFECTH
#define __BASICEFFECTH

#include "BaseEffect.h"

class CBasicEffect : public CBaseEffect
{
public:
	CBasicEffect(std::string PPTechName, std::string TechName, EPostProcesses PostProcess);
	~CBasicEffect();
	bool SetVariables();
	bool Update(float updateTime);
	bool Render(unsigned int pass);
	bool Release();
	std::string OutInfo();
private:
	CBasicEffect();
	CBasicEffect(const CBasicEffect&);
	CBasicEffect& operator= (const CBasicEffect&);
};

#endif