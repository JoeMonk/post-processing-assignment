#ifndef __SHAKEEFFECTH
#define __SHAKEEFFECTH

#include "BaseEffect.h"
#include <vector>
#include <sstream>
#include <string>

class CShakeEffect : public CBaseEffect
{
public:
	CShakeEffect(std::string PPTechName, std::string TechName);
	~CShakeEffect();

	static bool LoadVariables(ID3D10EffectScalarVariable* ShakeAmountVar, ID3D10EffectScalarVariable* ShakeXMoveVar, ID3D10EffectScalarVariable* ShakeYMoveVar);
	bool SetVariables();

	bool Update(float updateTime);

	bool ResetShake();

	bool Render(unsigned int pass);

	bool Release();

	std::string OutInfo();

private:
	CShakeEffect();
	CShakeEffect(const CShakeEffect&);
	CShakeEffect& operator= (const CShakeEffect&);

	static ID3D10EffectScalarVariable* m_ShakeAmountVar;
	static ID3D10EffectScalarVariable* m_ShakeXMoveVar;
	static ID3D10EffectScalarVariable* m_ShakeYMoveVar;

	float m_CurrentShake;
	float m_CurrentTime;
	float m_ShakeTime;	

	float m_ShakeSpeed;
	bool m_ShakeForwards;

	float m_MaxShakeLimit;

	float m_ShakeLimit;

	float m_XShake;
	float m_YShake;

	bool m_XForwards;	
	bool m_YForwards;

	float m_MaxShakeMove;

	float m_CurrentMaxXMove;
	float m_CurrentMaxYMove;

	float m_MoveMult;
};

#endif