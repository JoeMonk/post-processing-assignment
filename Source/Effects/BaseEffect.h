#ifndef __BASEEFFECTH
#define __BASEEFFECTH

#include <vector>
#include <algorithm>

#include <d3d10.h>
#include <d3dx10.h>

// Say what effects there are here
enum EPostProcesses
{
	eCopy, eCopyWorldArea, eTint, eBlur, eShake, eNegative, eSepia, eGrayscale, eDoF, eBloom,
	NumPostProcesses
};

class CBaseEffect
{
public:
	// Sets the variables of each effect in the shader
	virtual bool SetVariables() = 0;
	virtual bool Release() = 0;

	// Updates the variables of each effect ready to send to the shader
	virtual bool Update(float updateTime) = 0;

	bool SwitchProcess();	// Add process if not in the vector, remove it if it is.

	// Renders the scene using the effect
	virtual bool Render(unsigned int pass) = 0;

	virtual std::string OutInfo() = 0;

	// Sets the ID3D10EffectTechnique after loading it from the .fx file
	bool SetEffectTechnique(ID3D10EffectTechnique* effectTechnique);

	// Returns the effect loaded
	ID3D10EffectTechnique* GetEffectTechnique();

	// Returns the string describing what the technique is
	std::string GetTechName();

	// Returns the name of the technique in the .fx file
	std::string GetPPTechName();

	// Returns the vector containing the enum of the current techniques
	static std::vector<EPostProcesses>* GetTechniques();

	// Sets the current techniques vector, has to be done once before use
	static bool SetTechniques(std::vector<EPostProcesses>* currentTechniques);

	// Returns the ID3D10Device used for the techniques
	static ID3D10Device* GetDevice();

	// Sets the ID3D10Device used by the effects, has to be done once before use
	static bool SetDevice(ID3D10Device* g_pd3dDevice);

	bool GetAreaPP();
	void AreaPPOn();
	void AreaPPOff();

	static unsigned int CurrentTarget();
	static bool SwitchCurrentTarget();

	unsigned int GetPasses();

	// Getters and setters for the static resources needed for rendering
	static bool SetSceneTextureVar		(ID3D10EffectShaderResourceVariable*	SceneTextureVar);
	static bool SetSceneRenderTarget1	(ID3D10RenderTargetView*				SceneRenderTarget1);
	static bool SetSceneRenderTarget2	(ID3D10RenderTargetView*				SceneRenderTarget2);
	static bool SetSceneShaderResource1	(ID3D10ShaderResourceView*				SceneShaderResource1);
	static bool SetSceneShaderResource2	(ID3D10ShaderResourceView*				SceneShaderResource2);
	static bool SetDepthStencilView		(ID3D10DepthStencilView*				DepthStencilView);

	static ID3D10EffectShaderResourceVariable*	GetSceneTextureVar		();
	static ID3D10RenderTargetView*				GetSceneRenderTarget1	();
	static ID3D10RenderTargetView*				GetSceneRenderTarget2	();
	static ID3D10ShaderResourceView*			GetSceneShaderResource1 ();
	static ID3D10ShaderResourceView*			GetSceneShaderResource2 ();
	static ID3D10DepthStencilView*				GetDepthStencilView		();

	CBaseEffect(std::string PPTechName, std::string TechName, EPostProcesses effect, unsigned int passes);	// Set CurrentTechniques first
	~CBaseEffect();

private:
	// Hide the default constructors
	CBaseEffect();
	CBaseEffect(const CBaseEffect&);
	CBaseEffect& operator= (const CBaseEffect&);

	// A pointer to the current post processes, allows removal and addition of self
	// Same for all, set to static
	static std::vector<EPostProcesses>* m_CurrentTechniques;
	EPostProcesses m_Effect;		                           // The enum of the technique

	static ID3D10Device* m_pd3dDevice;
	ID3D10EffectTechnique* m_EffectTechnique;	                           // The actual technique to use
	std::string m_PPTechName;	                                           // The technique name according to the fx file
	std::string m_TechName;		                                           // A better human readable string for the technique
	unsigned int m_Passes;                                                 // How many passes are in the technique (usually 1)

	static unsigned int							m_CurrentTarget;           // Which target to render to, swaps

	static ID3D10EffectShaderResourceVariable*	m_SceneTextureVar;         // The texture in the shader for effects to... effect
	static ID3D10RenderTargetView*				m_SceneRenderTarget1;      // The 2 targets and resources for the 2 textures
	static ID3D10RenderTargetView*				m_SceneRenderTarget2;
	static ID3D10ShaderResourceView*			m_SceneShaderResource1;
	static ID3D10ShaderResourceView*			m_SceneShaderResource2;
	static ID3D10DepthStencilView*				m_DepthStencilView;        // The depth stencil for everything - don't need 2

	// True if area PP only
	bool m_AreaPP;
};

#endif