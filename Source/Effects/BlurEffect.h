#ifndef __BLUREFFECTH
#define __BLUREFFECTH

#include "BaseEffect.h"
#include <sstream>
#include <string>

class CBlurEffect : public CBaseEffect
{
public:
	CBlurEffect(std::string PPTechName, std::string TechName);
	~CBlurEffect();
	static bool LoadVariables(ID3D10EffectScalarVariable* BlurWeightVar, ID3D10EffectScalarVariable* BlurWidthVar);
	bool SetVariables();
	bool Update(float updateTime);
	bool Render(unsigned int pass);
	bool DrawPass(unsigned int pass);
	bool Release();
	std::string OutInfo();
	bool ChangeSigma(float sigma = 2.0f);
	float GetSigma();
	bool ChangeKernelSize(int kernelSize = 7);
	int GetKernelSize();
private:
	CBlurEffect();
	CBlurEffect(const CBlurEffect&);
	CBlurEffect& operator= (const CBlurEffect&);
	static ID3D10EffectScalarVariable* m_BlurWeightVar;
	static ID3D10EffectScalarVariable* m_BlurWidthVar;
	float m_GaussianSigma;
	int   m_GaussianKernelSize;
};

#endif