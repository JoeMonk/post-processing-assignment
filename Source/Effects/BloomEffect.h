#ifndef __BLOOMEFFECTH
#define __BLOOMEFFECTH

#include "BaseEffect.h"
#include "BlurEffect.h"	// Needs to be able to blur

class CBloomEffect : public CBaseEffect
{
public:
	CBloomEffect(std::string PPTechName, std::string TechName);
	~CBloomEffect();
	bool SetVariables();
	bool Update(float updateTime);
	bool Render(unsigned int pass);
	bool Release();
	static bool CreateTexture(unsigned int width, unsigned int height);
	static bool SetBlurEffect(CBlurEffect* blurEffect);

	static bool	SetVariables(
		ID3D10EffectShaderResourceVariable* BlurTextureVar,
		ID3D10EffectShaderResourceVariable* DepthTextureVar);

	static bool SetDepthStencilResource(ID3D10ShaderResourceView* m_DepthStencilResource);
	std::string OutInfo();
private:
	CBloomEffect();
	CBloomEffect(const CBloomEffect&);
	CBloomEffect& operator= (const CBloomEffect&);

	static ID3D10EffectShaderResourceVariable*  m_BlurTextureVar;
	static ID3D10Texture2D*						m_BlurTexture;
	static ID3D10RenderTargetView*				m_BlurRenderTarget;
	static ID3D10ShaderResourceView*			m_BlurShaderResource;

	static ID3D10EffectShaderResourceVariable*  m_DepthTextureVar;
	static ID3D10ShaderResourceView*			m_DepthStencilResource;

	static CBlurEffect*							m_BlurEffect;
};

#endif