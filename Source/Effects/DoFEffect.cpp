
#include "DoFEffect.h"

CDoFEffect::CDoFEffect(std::string PPTechName, std::string TechName) : CBaseEffect(PPTechName, TechName, eDoF, 1)
{
	m_DoFDistance = 105.0f;
	m_DoFRange = 15.0f;
}

CDoFEffect::~CDoFEffect()
{

}

bool CDoFEffect::Release()
{
	if (m_BlurShaderResource)	m_BlurShaderResource->Release();
	if (m_BlurRenderTarget)		m_BlurRenderTarget->Release();
	if (m_BlurTexture)			m_BlurTexture->Release();
	return true;
}

bool CDoFEffect::SetVariables(
	ID3D10EffectShaderResourceVariable* BlurTextureVar,
	ID3D10EffectShaderResourceVariable* DepthTextureVar,
	ID3D10EffectScalarVariable* NearClipVar,
	ID3D10EffectScalarVariable* FarClipVar,
	ID3D10EffectScalarVariable* DoFDistanceVar,
	ID3D10EffectScalarVariable* DoFRangeVar)
{
	m_BlurTextureVar = BlurTextureVar;
	m_DepthTextureVar = DepthTextureVar;
	m_NearClipVar = NearClipVar;
	m_FarClipVar = FarClipVar;
	m_DoFDistanceVar = DoFDistanceVar;
	m_DoFRangeVar = DoFRangeVar;
	return true;
}


bool CDoFEffect::SetDepthStencilResource(ID3D10ShaderResourceView* DepthStencilResource)
{
	m_DepthStencilResource = DepthStencilResource;
	return true;
}

ID3D10EffectShaderResourceVariable* CDoFEffect::m_BlurTextureVar(NULL);
CBlurEffect*						CDoFEffect::m_BlurEffect(NULL);
ID3D10ShaderResourceView*			CDoFEffect::m_DepthStencilResource(NULL);
ID3D10EffectShaderResourceVariable* CDoFEffect::m_DepthTextureVar(NULL);

ID3D10Texture2D*			CDoFEffect::m_BlurTexture(NULL);
ID3D10RenderTargetView*		CDoFEffect::m_BlurRenderTarget(NULL);
ID3D10ShaderResourceView*	CDoFEffect::m_BlurShaderResource(NULL);

ID3D10EffectScalarVariable* CDoFEffect::m_NearClipVar(NULL);
ID3D10EffectScalarVariable* CDoFEffect::m_FarClipVar(NULL);
ID3D10EffectScalarVariable* CDoFEffect::m_DoFDistanceVar(NULL);
ID3D10EffectScalarVariable* CDoFEffect::m_DoFRangeVar(NULL);
float CDoFEffect::m_NearClip(0);
float CDoFEffect::m_FarClip(0);

bool CDoFEffect::CreateTexture(unsigned int width, unsigned int height)
{
	m_BlurTexture;
	m_BlurRenderTarget;
	m_BlurShaderResource;

	D3D10_TEXTURE2D_DESC textureDesc;
	textureDesc.Width = width;  // Match views to viewport size
	textureDesc.Height = height;
	textureDesc.MipLevels = 1; // No mip-maps when rendering to textures (or we will have to render every level)
	textureDesc.ArraySize = 1;
	textureDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM; // RGBA texture (8-bits each)
	textureDesc.SampleDesc.Count = 1;
	textureDesc.SampleDesc.Quality = 0;
	textureDesc.Usage = D3D10_USAGE_DEFAULT;
	textureDesc.BindFlags = D3D10_BIND_RENDER_TARGET | D3D10_BIND_SHADER_RESOURCE; // Indicate we will use texture as render target, and pass it to shaders
	textureDesc.CPUAccessFlags = 0;
	textureDesc.MiscFlags = 0;
	if (FAILED(GetDevice()->CreateTexture2D(&textureDesc, NULL, &m_BlurTexture))) return false;

	// Get a "view" of the texture as a render target - giving us an interface for rendering to the texture
	if (FAILED(GetDevice()->CreateRenderTargetView(m_BlurTexture, NULL, &m_BlurRenderTarget))) return false;

	// And get a shader-resource "view" - giving us an interface for passing the texture to shaders
	D3D10_SHADER_RESOURCE_VIEW_DESC srDesc;
	srDesc.Format = textureDesc.Format;
	srDesc.ViewDimension = D3D10_SRV_DIMENSION_TEXTURE2D;
	srDesc.Texture2D.MostDetailedMip = 0;
	srDesc.Texture2D.MipLevels = 1;
	if (FAILED(GetDevice()->CreateShaderResourceView(m_BlurTexture, &srDesc, &m_BlurShaderResource))) return false;

	return true;
}

bool CDoFEffect::SetBlurEffect(CBlurEffect* blurEffect)
{
	m_BlurEffect = blurEffect;
	return true;
}

std::string CDoFEffect::OutInfo()
{
	std::stringstream sstream;
	sstream.precision(1);
	sstream << std::fixed;
	sstream << ": Distance = ";
	sstream << m_DoFDistance;
	sstream << std::endl;
	sstream << "Range = ";
	sstream << m_DoFRange;
	return sstream.str();
}

bool CDoFEffect::SetDoFDistance(float DoFDistance)
{
	m_DoFDistance = DoFDistance;
	return true;
}

float CDoFEffect::GetDoFDistance()
{
	return m_DoFDistance;
}

bool CDoFEffect::SetDoFRange(float DoFRange)
{
	m_DoFRange = DoFRange;
	return true;
}

float CDoFEffect::GetDoFRange()
{
	return m_DoFRange;
}

bool CDoFEffect::SetNearFarClip(float NearClip, float FarClip)
{
	m_NearClip = NearClip;
	m_FarClip = FarClip;
	return true;
}

bool CDoFEffect::SetVariables()
{
	m_NearClipVar->SetFloat(m_NearClip);
	m_FarClipVar->SetFloat(m_FarClip);
	m_DoFDistanceVar->SetFloat(m_DoFDistance);
	m_DoFRangeVar->SetFloat(m_DoFRange);
	return true;
}

bool CDoFEffect::Update(float updateTime)
{
	return true;
}

bool CDoFEffect::Render(unsigned int pass)
{
	ID3D10RenderTargetView* target;
	ID3D10ShaderResourceView* resource;
	ID3D10ShaderResourceView *const pSRV[1] = { NULL };
	GetDevice()->PSSetShaderResources(0, 1, pSRV);
	GetDevice()->PSSetShaderResources(1, 1, pSRV);
	GetDevice()->PSSetShaderResources(2, 1, pSRV);

	m_BlurEffect->SetVariables();

	// Render the blurred scene first pass to one scene texture
	if (CurrentTarget() == 1)
	{
		resource = GetSceneShaderResource2();
		target = GetSceneRenderTarget1();
	}
	else
	{
		resource = GetSceneShaderResource1();
		target = GetSceneRenderTarget2();
	}


	GetSceneTextureVar()->SetResource(resource);

	GetDevice()->OMSetRenderTargets(1, &target, GetDepthStencilView());

	m_BlurEffect->DrawPass(0);

	CBaseEffect::SwitchCurrentTarget();
	
	// Clear shader resources
	GetDevice()->PSSetShaderResources(0, 1, pSRV);
	GetDevice()->PSSetShaderResources(1, 1, pSRV);
	GetDevice()->PSSetShaderResources(2, 1, pSRV);
	// Render the blurred scene second pass to the blur texture
	// This is so we don't overwrite the original texture for the unblurred scene
	if (CurrentTarget() == 1)
	{
		resource = GetSceneShaderResource2();
	}
	else
	{
		resource = GetSceneShaderResource1();
	}

	GetSceneTextureVar()->SetResource(resource);

	GetDevice()->OMSetRenderTargets(1, &m_BlurRenderTarget, GetDepthStencilView());

	m_BlurEffect->DrawPass(1);
	
	// Clear shader resources
	GetDevice()->PSSetShaderResources(0, 1, pSRV);
	GetDevice()->PSSetShaderResources(1, 1, pSRV);
	GetDevice()->PSSetShaderResources(2, 1, pSRV);

	// Render the pass with DoF
	// Set unblurred scene resource and other scene texture as target.
	// Set blurred texture

	if (CurrentTarget() == 1)
	{
		resource = GetSceneShaderResource1();
		target = GetSceneRenderTarget2();
	}
	else
	{
		resource = GetSceneShaderResource2();
		target = GetSceneRenderTarget1();
	}
	// Set blurred resource
	GetDevice()->OMSetRenderTargets(1, &target, NULL);


	m_DepthTextureVar->SetResource(m_DepthStencilResource);
	GetSceneTextureVar()->SetResource(resource);
	m_BlurTextureVar->SetResource(m_BlurShaderResource);

	GetEffectTechnique()->GetPassByIndex(0)->Apply(0);

	GetDevice()->Draw(4, 0);

	GetDevice()->PSSetShaderResources(0, 1, pSRV);
	GetDevice()->PSSetShaderResources(1, 1, pSRV);
	GetDevice()->PSSetShaderResources(2, 1, pSRV);

	CBaseEffect::SwitchCurrentTarget();

	return true;
}