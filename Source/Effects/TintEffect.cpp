#include "TintEffect.h"

CTintEffect::CTintEffect(std::string PPTechName, std::string TechName) : CBaseEffect(PPTechName, TechName, eTint, 1)
{
	m_TintColour = gen::CVector3(0.0f, 1.0f, 0.5f);	// Starting colour in HSL
	m_TintSpeed = 0.05f;
	m_TintColourVar = NULL;
}

CTintEffect::~CTintEffect()
{
	
}

ID3D10EffectVectorVariable* CTintEffect::m_TintColourVar(NULL);

bool CTintEffect::Release()
{
	return true;
}

bool CTintEffect::LoadVariables(ID3D10EffectVectorVariable* TintColourVar)
{
	m_TintColourVar = TintColourVar;
	return true;
}

std::string CTintEffect::OutInfo()
{
	std::stringstream sstream;
	sstream.precision(3);
	sstream << std::fixed;
	sstream << m_TintColour.x;
	return std::string(": Hue = " + sstream.str());
}

bool CTintEffect::SetVariables()
{
	// Set the colour used to tint the scene
	int rgbColour[3] = { 0, 0, 0 };
	int hslColour[3] = { static_cast<int>(m_TintColour.x * 360.0f),
		static_cast<int>(m_TintColour.y * 100.0f),
		static_cast<int>(m_TintColour.z * 100.0f) };
	HSLToRGB(hslColour[0], hslColour[1], hslColour[2], rgbColour[0], rgbColour[1], rgbColour[2]);
	gen::CVector3 convertedRGB = gen::CVector3(static_cast<float>(rgbColour[0]) / 255.0f, static_cast<float>(rgbColour[1]) / 255.0f, static_cast<float>(rgbColour[2] / 255.0f));

	m_TintColourVar->SetRawValue(&convertedRGB, 0, 12);

	return true;
}

bool CTintEffect::Update(float updateTime)
{
	m_TintColour.x += m_TintSpeed * updateTime;
	if (m_TintColour.x >= 1.0f)
	{
		m_TintColour.x -= 1.0f;
	}

	return true;
}

bool CTintEffect::Render(unsigned int pass)
{
	ID3D10RenderTargetView* target;
	ID3D10ShaderResourceView *const pSRV[1] = { NULL };
	GetDevice()->PSSetShaderResources(0, 1, pSRV);

	if (CurrentTarget() == 1)
	{
		GetSceneTextureVar()->SetResource(GetSceneShaderResource2());
		target = GetSceneRenderTarget1();
	}
	else
	{
		GetSceneTextureVar()->SetResource(GetSceneShaderResource1());
		target = GetSceneRenderTarget2();
	}
	
	GetDevice()->OMSetRenderTargets(1, &target, GetDepthStencilView());

	GetEffectTechnique()->GetPassByIndex(0)->Apply(0);
	GetDevice()->Draw(4, 0);

	return true;
}