#ifndef __TINTEFFECTH
#define __TINTEFFECTH

#include "BaseEffect.h"
#include "CVector3.h"
#include "ColourConversions.h"
#include <sstream>
#include <string>

class CTintEffect : public CBaseEffect
{
public:
	CTintEffect(std::string PPTechName, std::string TechName);
	~CTintEffect();
	static bool LoadVariables(ID3D10EffectVectorVariable* TintColourVar);
	bool SetVariables();
	bool Update(float updateTime);
	bool Render(unsigned int pass);
	bool Release();
	std::string OutInfo();
private:
	CTintEffect();
	CTintEffect(const CTintEffect&);
	CTintEffect& operator= (const CTintEffect&);
	gen::CVector3 m_TintColour;
	float m_TintSpeed;
	static ID3D10EffectVectorVariable* m_TintColourVar;
};

#endif