//
// ColourConversions.cpp
//
// Source file for colour conversion functions. The key functions are initially
// empty - you need to implement them for the lab exercise

#include <math.h>

void HSLtoRGB_subfunction(int &c, float &temp1, float &temp2, float &temp3);

// Find the minimum of three numbers (helper function for exercise below)
float Min( float f1, float f2, float f3 )
{
	float fMin = f1;
	if (f2 < fMin)
	{
		fMin = f2;
	}
	if (f3 < fMin)
	{
		fMin = f3;
	}
	return fMin;
}

// Find the maximum of three numbers (helper function for exercise below)
float Max( float f1, float f2, float f3 )
{
	float fMax = f1;
	if (f2 > fMax)
	{
		fMax = f2;
	}
	if (f3 > fMax)
	{
		fMax = f3;
	}
	return fMax;
}

// Convert an RGB colour to a HSL colour
void RGBToHSL( int R, int G, int B, int& H, int& S, int& L )
{
	float fR = (static_cast<float>(R) / 255.0f);
	float fG = (static_cast<float>(G) / 255.0f);
	float fB = (static_cast<float>(B) / 255.0f);

	//Find the Max
	float max = Max(fR, fG, fB);

	//Find the Min
	float min = Min(fR, fG, fB);

	L = static_cast<int>(50 * (min + max));

	if (min == max)
	{
		S = 0;
		H = 0;
		return;
	}

	if (L < 50.0f)
	{
		S = static_cast<int>(100.0f * (max - min) / (max + min));
	}
	else
	{
		S = static_cast<int>(100.0f * (max - min) / (2.0f - max - min));
	}

	if (max == fR)
	{
		H = static_cast<int>(60.0f * (fG - fB) / (max - min));
	}
	if (max == fG)
	{
		H = static_cast<int>((60.0f * (fB - fR) / (max - min)) + 120.0f);
	}
	if (max == fB)
	{
		H = static_cast<int>((60.0f * (fR - fG) / (max - min)) + 240.0f);
	}

	if (H < 0.0f)
	{
		H = static_cast<int>(H + 360.0f);
	}
}

// Convert a HSL colour to an RGB colour
//http://forums.codeguru.com/showthread.php?421328-RGB-to-HSL-and-vice-versa
void HSLToRGB( int H, int S, int L, int& R, int& G, int& B )
{
	R = 0;
	G = 0;
	B = 0;

	float l = static_cast<float>(L) / 100.0f;
	float s = static_cast<float>(S) / 100.0f;
	float h = static_cast<float>(H) / 360.0f;

	if (S == 0)
	{
		R = static_cast<int>(l * 100.0f);
		G = static_cast<int>(l * 100.0f);
		B = static_cast<int>(l * 100.0f);
	}

	else
	{
		float temp1 = 0.0f;

		if (l < 0.5f)
		{
			temp1 = l * (1.0f + s);
		}
		else
		{
			temp1 = l + s - (l * s);
		}

		float temp2 = (2.0f * l) - temp1;

		float temp3 = 0.0f;

		//Red
		temp3 = h + (1.0f/3.0f);
		if (temp3 > 1.0f)
		{
			temp3 -= 1.0f;
		}
		HSLtoRGB_subfunction(R, temp1, temp2, temp3);

		//Green
		temp3 = h;
		HSLtoRGB_subfunction(G, temp1, temp2, temp3);

		//Blue
		temp3 = h - (1.0f / 3.0f);
		if (temp3 < 0.0f)
		{
			temp3 += 1.0f;
		}
		HSLtoRGB_subfunction(B, temp1, temp2, temp3);
	}
	R = static_cast<int> ((static_cast<float>(R) / 100.0f) * 255.0f);
	G = static_cast<int> ((static_cast<float>(G) / 100.0f) * 255.0f);
	B = static_cast<int> ((static_cast<float>(B) / 100.0f) * 255.0f);
}

void HSLtoRGB_subfunction(int &c, float &temp1, float &temp2, float &temp3)
{
	if ((temp3 * 6.0f) < 1.0f)
	{
		c = static_cast <int>((temp2 + (temp1 - temp2) * 6.0f * temp3) * 100.0f);
	}
	else if ((temp3 * 2.0f) < 1.0f)
	{
		c = static_cast <int>(temp1 * 100.0f);
	}
	else if ((temp3 * 3.0f) < 2.0f)
	{
		c = static_cast <int>((temp2 + (temp1 - temp2) * ((2.0f/3.0f) - temp3) * 6.0f) * 100.0f);
	}
	else
	{
		c = static_cast <int>(temp2 * 100.0f);
	}
}
